package edu.berkeley.cs162;

import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Comparator;

public class RangeMap<T> {
    private TreeMap<Long, T> map = new TreeMap<Long, T>(new Comparator<Long>() {
        public int compare(Long l1, Long l2) {
            if(l1 == l2)
                return 0;
            else if(isLessThanUnsigned(l1, l2))
                return -1;
            else
                return 1;
        }
    });
    
	private boolean isLessThanUnsigned(long n1, long n2) {
		return (n1 < n2) ^ ((n1 < 0) != (n2 < 0));
	}
	
	private boolean isLessThanEqualUnsigned(long n1, long n2) {
		return isLessThanUnsigned(n1, n2) || n1 == n2;
	}
    
    
    private Long [] getMapArray() {
        // keySet returns the value of keys in ascending order
        return map.keySet().toArray(new Long[map.size()]);
    }
    
    private int getIndex(long key) {
        Long [] arr = getMapArray();
        int i;
        
        if(arr.length == 0)
            return -1;
        
        for(i = 0; i < arr.length; i++)
            if (i > 0 && isLessThanUnsigned(arr[i-1], key) &&
                isLessThanEqualUnsigned(key, arr[i]))
                return i;
        
        if(isLessThanEqualUnsigned(key, arr[0]) ||
           isLessThanUnsigned(arr[arr.length - 1], key))
            return 0;

        return -1;
    }
    
    /**
     * @return the value that matches the range the key is in
     */
    public synchronized T get(long key) {
        Long [] arr = getMapArray();
        int index = getIndex(key);
        
        if (index < 0)
            return null;
        else
            return map.get(arr[index]);
    }
    
    /**
     * @return the same as get but key of the range next to it
     */
    public synchronized T getSuccessor(long key) {
        Long [] arr = getMapArray();
        int index = getIndex(key);
        
        if (index < 0)
            return null;
        else
            return map.get(arr[(index + 1) % arr.length]);
    }
    
    /*
     * Takes in a key and a value and creates a range ending at key
     */
    public synchronized T put(long key, T value) {
        return map.put(new Long(key), value);
    }
    
    public synchronized int size() {
        return map.size(); 
    }
}
