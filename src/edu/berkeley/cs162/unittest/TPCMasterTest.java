package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.regex.Matcher;

public class TPCMasterTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();
     @Rule public TestName name = new TestName();
        
    private TPCMaster tpcMaster = null;
    @Before
    public void setUp() {
        tpcMaster = new TPCMaster(0);
    }

    /*
     * @return a Socket with the msg as the reseponse
     */
    private Socket socketStub(KVMessage returnMsg) throws KVException {
        Socket socket = mock(Socket.class);
        ByteArrayInputStream output = new ByteArrayInputStream(returnMsg.toXML().getBytes());
        try {
            when(socket.getInputStream()).thenReturn(output);
            when(socket.getOutputStream()).thenReturn(new ByteArrayOutputStream());
        } catch (IOException e) {
        }
        
        return socket;
    }

    private Socket socketStubTimeout() {
        Socket socket = mock(Socket.class);
        try {
            when(socket.getOutputStream()).thenReturn(new ByteArrayOutputStream());
            doThrow(new SocketTimeoutException()).when(socket).getInputStream();
        } catch (IOException e) {
        }
        
        return socket;
    }

    private Socket socketStubUnknownHost() throws UnknownHostException {
        throw new UnknownHostException();
    }

    /*
     * HandleGet tests
     */

    @Test
    public void socketStubTest() throws Exception {
        KVMessage msg = new KVMessage(KVMessage.RESP, KVError.SUCCESS);
        Socket socket = socketStub(msg);
        KVMessage result = new KVMessage(socket);
        assertEquals(KVMessage.RESP, result.getMsgType());
        assertEquals(KVError.SUCCESS.toString(), result.getMessage());
    }

    @Test
    public void getFromSlaveTest() throws Exception {
        KVMessage resp = new KVMessage(KVMessage.RESP);
        resp.setKey("hello");
        resp.setValue("world");
        assertEquals("world", tpcMaster.getFromSlave("hello", socketStub(resp)));
    }

    @Test
    public void getFromSlaveNotFoundTest() throws Exception {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());

        KVMessage resp = new KVMessage(KVMessage.RESP);
        resp.setMessage(KVError.DOES_NOT_EXIST.toString());
        tpcMaster.getFromSlave("invalidkey", socketStub(resp));
    }

    @Test
    public void getFromSlaveTimeoutTest() throws Exception {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.TIMEOUT.toString());

        Socket socket = socketStubTimeout();
        tpcMaster.getFromSlave("timeout", socket);
    }

    @Test
    public void getValuesFromSlavesTest() throws Exception {
        String value = "value";
        assertEquals(value, tpcMaster.getValueFromSlaves(value, null, value, null));
        assertEquals(value, tpcMaster.getValueFromSlaves(value, null, null, null));
        assertEquals(value, tpcMaster.getValueFromSlaves(null, null, value, null));
    }
    
    @Test
    public void getValuesFromSlavesException1Test() throws Exception {
        String error1 = "error1";
        String error2 = "error2";
        exception.expect(KVException.class);
        exception.expectMessage(error1 + "\n" + error2);
        tpcMaster.getValueFromSlaves(null, error1, null, error2);
    }

    @Test
    public void getValuesFromSlavesException2Test() throws Exception {
        String error1 = "error1";
        exception.expect(KVException.class);
        exception.expectMessage(error1);
        tpcMaster.getValueFromSlaves(null, error1, null, null);
    }

    @Test
    public void getValuesFromSlavesException3Test() throws Exception {
        String error2 = "error2";
        exception.expect(KVException.class);
        exception.expectMessage(error2);
        tpcMaster.getValueFromSlaves(null, null, null, error2);
    }

    @Test
    public void handleGetTest() throws Exception {
        final String value = "value";
        tpcMaster = new TPCMaster(0) {
            @Override 
            public String getValueFromThreads(String key) {
                return value;         
            }
        }; 
        KVMessage request = new KVMessage(KVMessage.GETREQ);
        request.setKey("key");
        assertEquals(value, tpcMaster.handleGet(request));
        assertEquals(value, tpcMaster.handleGet(request));
        assertEquals(value, tpcMaster.handleGet(request));
        assertEquals(value, tpcMaster.handleGet(request));
    }

    /*
     * HandleTPCOperation tests
     */

    /*
     * Phase One
     */
    
    private class PhaseOneStub extends TPCMaster {
        public PhaseOneStub(int i) {
            super(i);
        }
        public void sendPrepare(KVMessage msg, Socket replica) {
        }

        public void slaveCloseHost(Object obj, Socket replica) {
        }

    }

    private class PhaseOneReady extends PhaseOneStub {
        public PhaseOneReady(int i) {
            super(i);
        }
        public Socket slaveConnectHost(Object obj) throws KVException {
            KVMessage ready = new KVMessage(KVMessage.READY);
            ready.setTpcOpId("1");
            return socketStub(ready);
        }
        public void barrier() {
        }
    }

    private class PhaseOneReadyWithBarrier extends PhaseOneStub {
        public PhaseOneReadyWithBarrier(int i) {
            super(i);
        }
        public Socket slaveConnectHost(Object obj) throws KVException {
            KVMessage ready = new KVMessage(KVMessage.READY);
            ready.setTpcOpId("1");
            return socketStub(ready);
        }
    }

    private class PhaseOneTimeout extends PhaseOneStub {
        public PhaseOneTimeout(int i) {
            super(i);
        }
        public Socket slaveConnectHost(Object obj) throws KVException {
            return socketStubTimeout();
        }
        public void barrier() {
        }
    }


    private class PhaseOneInvalidHost extends PhaseOneStub {
        public PhaseOneInvalidHost(int i) {
            super(i);
        }
        public Socket slaveConnectHost(Object obj) throws KVException {
            throw new KVException(KVMessage.RESP, "Invalid Host");
        }
        public void barrier() {
        }
    
    }

    private class PhaseOneAbort extends PhaseOneStub {
        public PhaseOneAbort(int i) {
            super(i);
        }
        public Socket slaveConnectHost(Object obj) throws KVException {
            KVMessage abort = new KVMessage(KVMessage.ABORT);
            abort.setTpcOpId("1");
            return socketStub(abort);
        }
        public void barrier() {
        }
    }

    @Test 
    public void phaseOneReadyTest() throws Exception {
        // sends a msg to phaseOne and slave returns a ready response
        tpcMaster = new PhaseOneReady(0);
        assertEquals(KVMessage.READY, tpcMaster.firstPhase(null, null).getMsgType());
    }


    @Test 
    public void phaseOneAbortTest() throws Exception {
        // sends a msg to phaseOne and slave returns a ready response
        tpcMaster = new PhaseOneAbort(0);

        assertEquals(KVMessage.ABORT, tpcMaster.firstPhase(null, null).getMsgType());
    }

    @Test 
    public void phaseOneAbortTimeoutTest() throws Exception {
        // sends a msg to phaseOne and slave returns a ready response
        tpcMaster = new PhaseOneTimeout(0);

        assertEquals(null, tpcMaster.firstPhase(null, null));
        assertEquals(true, tpcMaster.getGlobalAbort());
    }

    @Test 
    public void phaseOneAbortInvalidHostTest() throws Exception {
        // sends a msg to phaseOne and slave returns a ready response
        tpcMaster = new PhaseOneInvalidHost(0);

        assertEquals(null, tpcMaster.firstPhase(null, null));
        assertEquals(true, tpcMaster.getGlobalAbort());
    }

    private class PhaseOneThread extends Thread {
        private boolean abort;
        private TPCMaster tpcMaster;
        private KVMessage value;
        public PhaseOneThread(TPCMaster tpcMaster, boolean abort) {
            this.abort = abort;
            this.tpcMaster = tpcMaster;
        }

        public void run() {
            try {
                value = tpcMaster.firstPhase(null, null, abort);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } 

        public KVMessage getValue() {
            return value;
        }

        public boolean isGlobalAbort() {
            return tpcMaster.getGlobalAbort(); 
        }
    }

    @Test
    public void phaseOneThreadedSingleAbort() throws Exception {
        tpcMaster = new PhaseOneReadyWithBarrier(0);
        PhaseOneThread t1 = new PhaseOneThread(tpcMaster, false);
        PhaseOneThread t2 = new PhaseOneThread(tpcMaster, true);

        t1.start();
        t2.start();
        t1.join();
        t2.join();

        assertTrue(t1.isGlobalAbort());
        assertTrue(t2.isGlobalAbort());
        assertEquals(null, t1.getValue());
        assertEquals(null, t2.getValue());
    }

    @Test
    public void phaseOneThreadedAllAbort() throws Exception {
        tpcMaster = new PhaseOneReadyWithBarrier(0);
        PhaseOneThread t1 = new PhaseOneThread(tpcMaster, true);
        PhaseOneThread t2 = new PhaseOneThread(tpcMaster, true);

        t1.start();
        t2.start();
        t1.join();
        t2.join();

        assertTrue(t1.isGlobalAbort());
        assertTrue(t2.isGlobalAbort());
        assertEquals(null, t1.getValue());
        assertEquals(null, t2.getValue());
    }

    @Test
    public void phaseOneThreadedNoAbort() throws Exception {
        tpcMaster = new PhaseOneReadyWithBarrier(0);
        PhaseOneThread t1 = new PhaseOneThread(tpcMaster, false);
        PhaseOneThread t2 = new PhaseOneThread(tpcMaster, false);

        t1.start();
        t2.start();
        t1.join();
        t2.join();

        assertFalse(t1.isGlobalAbort());
        assertFalse(t2.isGlobalAbort());
        assertEquals(KVMessage.READY, t1.getValue().getMsgType());
        assertEquals(KVMessage.READY, t2.getValue().getMsgType());
    }

    /*
     * Phase two
     */

    @Test 
    public void phaseTwoReadyTest() throws Exception {
        // devision was commit and receive a successful ack
        tpcMaster = new TPCMaster(0) {
            public Socket slaveConnectHost(Object obj) throws KVException {
                KVMessage ack = new KVMessage(KVMessage.ACK);
                ack.setTpcOpId("1");
                return socketStub(ack);
            }

            public void sendPrepare(KVMessage msg, Socket replica) {
            }

            public void slaveCloseHost(Object obj, Socket replica) {
            }

            public void slaveSetSlave(Object obj, String key, boolean isSuccessor) {
            }
        };

        KVMessage replicaResp = new KVMessage(KVMessage.COMMIT);
        replicaResp.setTpcOpId("1");

        tpcMaster.secondPhase(null, replicaResp, false, null);
    }

    @Test 
    public void phaseTwoAbortTest() throws Exception {
        // decision was abort and receive a successful ack
        exception.expect(KVException.class);
        exception.expectMessage("abort");

        tpcMaster = new TPCMaster(0) {
            public Socket slaveConnectHost(Object obj) throws KVException {
                KVMessage ack = new KVMessage(KVMessage.ACK);
                ack.setTpcOpId("1");
                return socketStub(ack);
            }

            public void sendPrepare(KVMessage msg, Socket replica) {
            }

            public void slaveCloseHost(Object obj, Socket replica) {
            }

            public void slaveSetSlave(Object obj, String key, boolean isSuccessor) {
            }
        };

        KVMessage replicaResp = new KVMessage(KVMessage.ABORT);
        replicaResp.setMessage("abort");
        replicaResp.setTpcOpId("1");

        tpcMaster.secondPhase(null, replicaResp, false, null);
    }
}
