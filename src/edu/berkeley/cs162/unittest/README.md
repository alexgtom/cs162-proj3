Unit Tests
==========

Testing Tools
-------------
 * [JUnit4](https://github.com/dsaff/junit) -- Unit test framework
 * [mockitto](http://code.google.com/p/mockito/) -- For stubbing and mocking 
   functionality.
 * [Cobertura](http://cobertura.sourceforge.net/) -- For code coverage

Running unit tests
---------------------
Run ``make`` in this directory

Running code coverage
---------------------
Run ``make coverage`` in this directory. Open `coverage/index.html` to view 
the report.
