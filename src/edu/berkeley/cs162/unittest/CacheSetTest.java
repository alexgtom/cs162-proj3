package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import java.io.File;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;

public class CacheSetTest {


    @Test
    public void testConstructor() {
    	CacheSet.test1();
    }
    
    @Test
    public void testPutSimple() {
    	CacheSet.test2();
    }
    
    @Test
    public void testPutOverwrite() {
    	CacheSet.test3();
    }
    
    @Test
    public void testGetExist() {
    	CacheSet.test4();
    }
    
    @Test
    public void testGetMissing() {
    	CacheSet.test5();
    }
    
    @Test
    public void testEvictionConsistency() {
    	CacheSet.test6();
    }
    
    @Test
    public void testEvictionChoice() {
    	CacheSet.test7();
    }
    
    @Test
    public void testDeleteExist() {
    	CacheSet.test8();
    }

    @Test
    public void testDeleteMissing() {
    	CacheSet.test9();
    }
    
    @Test
    public void testPutReplace() {
    	CacheSet.test10();
    }

    @Test
    public void testPutOverwrite2() {
	CacheSet.test11();
    }

}

