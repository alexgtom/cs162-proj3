package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;

public class KVServer2Test {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private String oversize() {
        String result = new String();
        for (int i = 0; i< 257; i++) {
            result = result + "i";
        }
        return result;
    }

    @Test
    public void putAndGet2Test() throws KVException {
        KVServer cache = new KVServer(100, 2);
        for(int i = 0; i < 200; i++)
            cache.put(i + "", i + "");
    
        for(int i = 0; i < 200; i++)
            assertEquals(i + "", cache.get(i + ""));
    
        for(int i = 200; i < 400; i++)
            cache.put(i + "", i + "");
    
        for(int i = 200; i < 400; i++)
            assertEquals(i + "", cache.get(i + ""));
    }

    @Test
    public void putKeyOversizeTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_KEY.toString());
        KVServer server = new KVServer(1,10);
        String oskey = this.oversize();
        server.put(oskey, String.valueOf(10));
    }


    @Test
    public void getOverwriteTest() throws KVException {
        KVServer server = new KVServer(1,10);
        server.put("162", "cs162");
        assertEquals("cs162", server.get(String.valueOf(162)));
        server.put("162", "cs184");
        assertEquals("cs184", server.get(String.valueOf(162)));
    }

    @Test
    public void delTest() throws KVException {
        exception.expect(KVException.class);
        //need to be implemented in server then this test can pass
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        KVServer server = new KVServer(1,10);
        server.put("162", "cs162");
        assertEquals("cs162", server.get(String.valueOf(162)));
        server.del("162");
        String nonexist = server.get("162");
    }
    @Test
    public void delNonexistKeyTest() throws KVException {
        // need to be implemented in server then this test can pass
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        KVServer server = new KVServer(1,10);
        server.del("162");
    }

/*
 * Y = exists in store
 * N = does not exist in store
 * y = exists in cache
 * n = does not exist in cache
 * 3 possible situations: Yn, Yy, Nn. Ny is not possible.
 */
    
    private void fillServer(KVServer server, int cacheSize, 
            Map<Integer,Integer> cache, Map<Integer,Integer> store) throws KVException {
        for (int i = 0; i < cacheSize*2; i++) {
            server.put(String.valueOf(i),String.valueOf(i+100));
            store.put(i,i+100);
            if (i >= cacheSize) {
                cache.put(i,i+100);
            }
        }
        
    }
    
    private int getAbsentElement(Map<Integer,Integer> kv) {
        int absent = 0;
        for (int k : kv.keySet()) {
            absent = Math.max(k, absent);
        }
        return absent + 1;
    }
  
    @Test
    public void getYnTest() throws KVException{
        KVServer server = new KVServer(1,10);
        Map<Integer,Integer> cache = new HashMap<Integer, Integer>(10);
        Map<Integer,Integer> store = new HashMap<Integer, Integer>(20);
        try {
            fillServer(server, 10, cache, store);
        } catch (KVException e) {
            fail();
        }
        int inStoreNotCache = -1;
        for (Map.Entry<Integer,Integer> storeEntry : store.entrySet()) {
            if (!cache.containsKey(storeEntry.getKey())) {
                inStoreNotCache = storeEntry.getKey();
            }
        }
        assert inStoreNotCache != -1;
        String returned = server.get(String.valueOf(inStoreNotCache));
        String expected = String.valueOf(store.get(inStoreNotCache));
        assertTrue(returned.equals(expected));
        
    }

    @Test
    public void getYyTest() throws KVException{
        KVServer server = new KVServer(1,10);
        Map<Integer,Integer> cache = new HashMap<Integer, Integer>(10);
        Map<Integer,Integer> store = new HashMap<Integer, Integer>(20);
        try {
            fillServer(server, 10, cache, store);
        } catch (KVException e) {
            fail();
        }
        int inBoth = -1;
        for (Map.Entry<Integer,Integer> storeEntry : store.entrySet()) {
            if (cache.containsKey(storeEntry.getKey())) {
                inBoth = storeEntry.getKey();
            }
        }
        assert inBoth != -1;
        String returned = server.get(String.valueOf(inBoth));
        String expected = String.valueOf(store.get(inBoth));
        assertTrue(returned.equals(expected));
    }

    @Test
    public void getNnTest() throws KVException {
        exception.expect(KVException.class);
        KVServer server = new KVServer(1,10);
        Map<Integer,Integer> cache = new HashMap<Integer, Integer>(10);
        Map<Integer,Integer> store = new HashMap<Integer, Integer>(20);
        try {
            fillServer(server, 10, cache, store);
        } catch (KVException e) {
            fail();
        }
        int notIn = getAbsentElement(store);
        server.get(String.valueOf(notIn));

    }

  

}
