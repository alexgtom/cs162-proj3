package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;
import java.util.regex.Matcher;

class ClientThread extends Thread {
    private static int count = 0;
    private static final int range = 100;
    private KVClient client = new KVClient("localhost", 8080);
    private int begin;
    private int end;
    private int id;
    private static int idCount = 0;

    private static boolean DEBUG = true;

    public ClientThread() {
        begin = count;
        count += range;
        end = count;
        id = idCount;
        idCount++;
    }

    public void print(String s) {
        if(DEBUG)
            System.out.println("ClientThread #" + id + " : " + s);
    }

    public void run() {
        final int NUM_ITERATIONS = 1;
        for(int iteration = 0; iteration < NUM_ITERATIONS; iteration++) {
            print("Iteration " + iteration);
            for(int i = begin; i < end; i++) {
                print("PUT (" + i + ", " + i + ")");
                try {
                    client.put("" + i, "" + i);
                } catch (KVException e) {
                    System.out.println(e.getMessage());
                }
            }

            try {
                sleep(randomInt());
            } catch (InterruptedException e) {
            }

            for(int i = begin; i < end; i++) {
                print("GET " + i);
                try {
                    assertEquals(client.get("" + i), "" + i);
                } catch (KVException e) {
                    System.out.println(e.getMessage());
                }
            }

            try {
                sleep(randomInt());
            } catch (InterruptedException e) {
            }

            for(int i = begin; i < end; i++) {
                print("DEL " + i);
                try {
                    client.del("" + i);
                } catch (KVException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public int randomInt() {
        int min = 0;
        int max = 5;


        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }

	public static void delay(long sleepTime) {
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
		}
	}
}

public class StressTest {
    public static void main(String [] args) {
        final int NUM_CLIENTS = 10;
        ClientThread.delay(5000);
        for(int i = 0; i < NUM_CLIENTS; i++)
            (new ClientThread()).start();
    }
}
