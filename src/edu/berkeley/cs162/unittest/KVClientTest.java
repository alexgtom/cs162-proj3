package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.util.concurrent.CyclicBarrier;
import java.io.*;
public class KVClientTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();
	private String stringGenerator(int size) {
        byte[] arr = new byte[size];
        return new String(arr);
    }

    @Test
    public void PutAndGetSuccessTest() throws KVException {
        KVClient client = new KVClient("localhost",8080);
        client.put("Jason", "Peng");
        assertEquals("Peng", client.get("Jason"));
        client.put("Jason", "Peng");
        assertEquals("Peng", client.get("Jason"));
        client.put("Jason", "Peng");
        assertEquals("Peng", client.get("Jason"));
        client.put("Jason", "P");
        assertEquals("P", client.get("Jason"));
    }

    @Test
    public void PutAndGetAndDelSuccessTest() throws KVException {
        KVClient client = new KVClient("localhost",8080);
        client.put("Jason", "Peng");
        client.put("Yang", "Peng");
        assertEquals("Peng", client.get("Jason"));
        assertEquals("Peng", client.get("Yang"));
        client.del("Jason");
        client.del("Yang");
    }

    @Test
    public void PutOverrideTest() throws KVException {
        KVClient client = new KVClient("localhost",8080);
        client.put("Jason", "Peng");
        client.get("Jason");
        client.put("Jason", "King");
        assertEquals("King", client.get("Jason"));

    }

    @Test
    public void PutSamePairTwiceTest() throws KVException{
    	KVClient client = new KVClient("localhost",8080);
        client.put("Jason", "Peng");
        client.put("Jason", "Peng");
        client.put("Peng", "Jason");
        client.put("Peng", "Jason");
        assertEquals("Peng", client.get("Jason"));
        assertEquals("Jason", client.get("Peng"));
    }

  /* fail tests */
  
    @Test
    public void SocketConnectionErrorTest() throws KVException {
    	exception.expect(KVException.class);
        exception.expectMessage(KVError.NETWORK_ERROR_CONNECT.toString());
        KVClient client = new KVClient("remotehost", 1000);
        client.put("Jason", "Peng");
    }
    
    @Test
    public void GetFailTest1() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        KVClient client = new KVClient("localhost",8080);
        client.get("Jason");
    }
    
    @Test
    public void GetFailTest2() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        KVClient client = new KVClient("localhost",8080);
        client.put("Jason", "Peng");
        client.put("Yang", "Peng");
        client.get("Peng");
    }
    @Test
    public void GetFailTest3() throws KVException{
    	exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        KVClient client = new KVClient("localhost",8080);
        client.put("Jason", "Peng");
        client.put("Yang", "Peng");
        client.del("Jason");
        client.del("Yang");
        client.get("Jason");
    }
    @Test
    public void DelFailTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        KVClient client = new KVClient("localhost",8080);
        client.put("Jason", "Peng");
        client.put("Yang", "Peng");
        client.del("Jason");
        client.del("Jason");
    }
  
    @Test
    public void DelFail2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        KVClient client = new KVClient("localhost",8080);
        client.del("Jason");
    }

    @Test
    public void putOversizedKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_KEY.toString());
        KVClient client = new KVClient("localhost",8080);
        String key = stringGenerator(257);
        String val = stringGenerator(1);
        client.put(key, val);
    }
    
    @Test
    public void putOversizedValueTest() throws KVException {
       
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_VALUE.toString());
        KVClient client = new KVClient("localhost",8080);
        String key = stringGenerator(1);
        String val = stringGenerator(256*1024+1);
        client.put(key, val);
    }
    
    @Test
    public void getOversizedKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_KEY.toString());
        
        KVClient client = new KVClient("localhost",8080);
        String key = stringGenerator(257);
        client.get(key);
    }

    @Test
    public void multiplePutTest() throws KVException {
        KVClient client1 = new KVClient("localhost",8080);
        KVClient client2 = new KVClient("localhost",8080);
        client1.put("hello", "world");
        client2.put("hello", "1");
        assertEquals("1", client1.get("hello"));
        assertEquals("1", client2.get("hello"));
    }

    @Test
    public void multipleDeleteKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());

        KVClient client1 = new KVClient("localhost",8080);
        client1.put("hello", "world");
        KVClient client2 = new KVClient("localhost",8080);
        client1.del("hello");
        client2.del("hello");
    }
}
