package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;

public class KVServerTest {
    
    @Rule
    public ExpectedException exception = ExpectedException.none();
    
    // ===================== Start test utility methods =====================
    private String stringGenerator(int size) {
        byte[] arr = new byte[size];
        return new String(arr);
    }
    // ====================== End test utility methods ======================
    
    // ===================== Start unit tests for put() =====================
    @Test
    public void putOversizedKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_KEY.toString());
        KVServer server = new KVServer(1, 10);
        String key = stringGenerator(257);
        String val = stringGenerator(1);
        server.put(key, val);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void putNullKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());
        KVServer server = new KVServer(1, 10);
        String key = null;
        String val = stringGenerator(1);
        server.put(key, val);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void putEmptyKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());
        KVServer server = new KVServer(1, 10);
        String key = "";
        String val = stringGenerator(1);
        server.put(key, val);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void putOversizedValueTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_VALUE.toString());
        KVServer server = new KVServer(1, 10);
        String key = stringGenerator(1);
        String val = stringGenerator(256*1024+1);
        server.put(key, val);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void putNullValueTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_VALUE.toString());
        KVServer server = new KVServer(1, 10);
        String key = stringGenerator(1);
        String val = null;
        server.put(key, val);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void putEmptyValueTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_VALUE.toString());
        KVServer server = new KVServer(1, 10);
        String key = stringGenerator(1);
        String val = "";
        server.put(key, val);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void putNewKVPairTest() {
        KVServer server = new KVServer(1, 10);
        String key = "a";
        String val = "1";
        try {
            server.put(key, val);
            assertEquals("Wrong dataCache value!", "1", server.getCache().get(key));
            assertEquals("Wrong dataStore value!", "1", server.getStore().get(key));
        } catch (Exception e) {
            Assert.fail("Server threw an exception: " + e.getMessage());
        }
    }
    
    @Test
    public void putOverwriteKVPairTest() {
        KVServer server = new KVServer(1, 10);
        String key = "a";
        String val = "1";
        try {
            server.put(key, val);
            assertEquals("Wrong dataCache value!", "1", server.getCache().get(key));
            assertEquals("Wrong dataStore value!", "1", server.getStore().get(key));
            val = "2";
            server.put(key, val);
            assertEquals("Wrong dataCache value!", "2", server.getCache().get(key));
            assertEquals("Wrong dataStore value!", "2", server.getStore().get(key));
        } catch (Exception e) {
            Assert.fail("Server threw an exception: " + e.getMessage());
        }
    }

    @Test
    public void multiplePutTest() throws Exception {
        KVServer server = new KVServer(1, 10);
        String key = "a";
        String val = "1";
        server.put(key, val);
        server.put(key, val);
        server.put(key, val);
        server.put(key, val);
        server.put(key, val);
        server.put(key, val);
        server.put(key, val);

        assertEquals(val, server.get(key));
        assertEquals(val, server.get(key));
        assertEquals(val, server.get(key));
        assertEquals(val, server.get(key));
    
    }
    // ====================== End unit tests for put() ======================
    
    // ===================== Start unit tests for get() =====================
    @Test
    public void getOversizedKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_KEY.toString());
        KVServer server = new KVServer(1, 10);
        String key = stringGenerator(257);
        server.get(key);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void getNullKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());
        KVServer server = new KVServer(1, 10);
        String key = null;
        server.get(key);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void getEmptyKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());
        KVServer server = new KVServer(1, 10);
        String key = "";
        server.get(key);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void getKVPairFromCacheTest() {
        KVServer server = new KVServer(1, 10);
        String key = "a";
        String val = "1";
        try {
            server.put(key, val);
            String rtn = server.get(key);
            assertEquals("Wrong return value from dataCache!", "1", rtn);
        } catch (Exception e) {
            Assert.fail("Server threw an exception: " + e.getMessage());
        }
    }
    
    @Test
    public void getKVPairFromStoreTest() {
        KVServer server = new KVServer(1, 10);
        String key = "a";
        String val = "1";
        try {
            server.put(key, val);
            server.getCache().del(key);
            String rtn = server.get(key);
            assertEquals("Wrong return value from dataStore!", "1", rtn);
            assertEquals("Wrong value in dataCache!", "1", server.getStore().get(key));
        } catch (Exception e) {
            Assert.fail("Server threw an exception: " + e.getMessage());
        }
    }
    
    @Test
    public void getNullKVPairTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        KVServer server = new KVServer(1, 10);
        String key = "a";
        server.get(key);
        Assert.fail("Server did not throw an exception!");
    }
    // ====================== End unit tests for get() ======================
    
    // ===================== Start unit tests for del() =====================
    @Test
    public void delOversizedKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_KEY.toString());
        KVServer server = new KVServer(1, 10);
        String key = stringGenerator(257);
        server.del(key);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void delNullKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());
        KVServer server = new KVServer(1, 10);
        String key = null;
        server.del(key);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void delEmptyKeyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());
        KVServer server = new KVServer(1, 10);
        String key = "";
        server.del(key);
        Assert.fail("Server did not throw an exception!");
    }
    
    @Test
    public void delKVPairTest() {
        KVServer server = new KVServer(1, 10);
        String key = "a";
        String val = "1";
        try {
            server.put(key, val);
            server.del(key);
            assertEquals("Wrong value in dataCache!", null, server.getCache().get(key));
            try {
                server.getStore().get(key);
            } catch (Exception e) {
                assertTrue("Exception thrown is not an KVException!", e instanceof KVException);
                assertEquals("Wrong KVException thrown!", KVError.DOES_NOT_EXIST.toString(), e.getMessage());
            }
        } catch (Exception e) {
            Assert.fail("Server threw an exception: " + e.getMessage());
        }
    }
    
    @Test
    public void delKVPairFromStoreTest() {
        KVServer server = new KVServer(1, 10);
        String key = "a";
        String val = "1";
        try {
            server.put(key, val);
            server.getCache().del(key);
            server.del(key);
            assertEquals("Wrong value in dataCache!", null, server.getCache().get(key));
            try {
                server.getStore().get(key);
            } catch (Exception e) {
                assertTrue("Exception thrown is not an KVException!", e instanceof KVException);
                assertEquals("Wrong KVException thrown!", KVError.DOES_NOT_EXIST.toString(), e.getMessage());
            }
        } catch (Exception e) {
            Assert.fail("Server threw an exception: " + e.getMessage());
        }
    }
    
    @Test
    public void delNullKVPairTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        KVServer server = new KVServer(1, 10);
        String key = "a";
        server.del(key);
        Assert.fail("Server did not throw an exception!");
    }
    // ====================== End unit tests for del() ======================
    
}
