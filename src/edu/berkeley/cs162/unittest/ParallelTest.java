package edu.berkeley.cs162.unittest;

import java.net.Socket;

import edu.berkeley.cs162.KVException;
import edu.berkeley.cs162.KVMessage;
import edu.berkeley.cs162.KVServer;
import edu.berkeley.cs162.TPCMasterHandler;
import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;
public class ParallelTest {
	
	@Test 
	public void testParallelPutDelGet() throws Exception{
		KVMessage tpcReq = new KVMessage(KVMessage.PUTREQ);
		KVMessage tpcReq2 = new KVMessage(KVMessage.PUTREQ);
		KVMessage tpcReq3 = new KVMessage(KVMessage.PUTREQ);
		
		Runnable pu1 = new putParallel(tpcReq, "a", "a1", 40001);
		Runnable pu2 = new putParallel(tpcReq2, "b", "b1", 40002);
		Runnable pu3 = new putParallel(tpcReq3, "c", "c1", 40003);
		Thread thread1 = new Thread(pu1);
	    Thread thread2 = new Thread(pu2);
	    Thread thread3 = new Thread(pu3);
		thread1.start();

		thread2.start();
		thread3.start();

		thread1.join();
		thread2.join();
		thread3.join();
		KVMessage dReq = new KVMessage(KVMessage.DELREQ);
		KVMessage dReq2 = new KVMessage(KVMessage.DELREQ);
	
		
		Runnable d1= new deleteParallel(tpcReq, "a", 40004);
		Runnable d2= new deleteParallel(tpcReq, "b", 40005);
		
		Thread threadd1 = new Thread(d1);
	    Thread threadd2 = new Thread(d2);
		threadd1.start();
		threadd2.start();
		threadd1.join();
		threadd2.join();
		KVMessage ge1 = new KVMessage(KVMessage.GETREQ);
		KVMessage ge2= new KVMessage(KVMessage.GETREQ);
		KVMessage ge3 = new KVMessage(KVMessage.GETREQ);
		
		Runnable get1 = new getParallel(ge1, "a", 40006, KVError.DOES_NOT_EXIST.toString());
		Runnable get2 = new getParallel(ge2, "b", 40007, KVError.DOES_NOT_EXIST.toString());
		Runnable get3 = new getParallel(ge3, "c", 40008, "c1");
		Thread threadg1 = new Thread(get1);
	    Thread threadg2 = new Thread(get2);
	    Thread threadg3 = new Thread(get3);

	    threadg1.start();

		threadg2.start();
		threadg3.start();

		threadg1.join();
		threadg2.join();
		threadg3.join();
		
	}
	@Test
	public void testParallelPutGet()throws Exception{
		KVMessage tpcReq = new KVMessage(KVMessage.PUTREQ);
		KVMessage tpcReq2 = new KVMessage(KVMessage.PUTREQ);
		KVMessage tpcReq3 = new KVMessage(KVMessage.PUTREQ);
		
		Runnable pu1 = new putParallel(tpcReq, "hello", "world", 40011);
		Runnable pu2 = new putParallel(tpcReq2, "hello2", "world2", 40012);
		Runnable pu3 = new putParallel(tpcReq3, "hello3", "world3", 40013);
	    Thread thread1 = new Thread(pu1);
	    Thread thread2 = new Thread(pu2);
	    Thread thread3 = new Thread(pu3);

	    thread1.start();

		thread2.start();
		thread3.start();

		thread1.join();
		thread2.join();
		thread3.join();
		KVMessage ge1 = new KVMessage(KVMessage.GETREQ);
		KVMessage ge2= new KVMessage(KVMessage.GETREQ);
		KVMessage ge3 = new KVMessage(KVMessage.GETREQ);
		
		Runnable get1 = new getParallel(ge1, "hello", 40014, "world");
		Runnable get2 = new getParallel(ge2, "hello2", 40015, "world2");
		Runnable get3 = new getParallel(ge3, "hello3", 40016, "world3");
		
		Thread threadg1 = new Thread(get1);
	    Thread threadg2 = new Thread(get2);
	    Thread threadg3 = new Thread(get3);

	    threadg1.start();

		threadg2.start();
		threadg3.start();

		threadg1.join();
		threadg2.join();
		threadg3.join();
	}
	
	

}
class deleteParallel implements Runnable{
	KVMessage requestMsg;
	String key;
	Socket socket;
	int port;
	public deleteParallel( KVMessage requestMsg,
	String key, int port){
		this.requestMsg= requestMsg;
		this.key = key;
		this.port = port;
	}

	private Socket connectToSlave() throws Exception {
        return new Socket("localhost", port);
    }
	private void closeSlave(Socket slave) throws Exception {
        slave.close();
    }
	public void run(){
		try{
			socket = connectToSlave();
			requestMsg.setKey(key);
			requestMsg.sendMessage(socket);
			        
			KVMessage responseMsg = new KVMessage(socket.getInputStream());
			closeSlave(socket);
			socket = connectToSlave();
			KVMessage decis = new KVMessage(KVMessage.COMMIT);
			decis.sendMessage(socket);
		
			      
			KVMessage ack = new KVMessage(socket.getInputStream());
			closeSlave(socket);
			
			
			} catch(Exception e){
				
			}
	}
}

class putParallel implements Runnable{
	KVMessage requestMsg;
	String key;
	String value;
	Socket socket;
	int port;
	public putParallel( KVMessage requestMsg, String key, String value, int port){
		this.requestMsg= requestMsg;
		this.key = key;
		this.port = port;
		this.value = value;
	}

	private Socket connectToSlave() throws Exception {
        return new Socket("localhost", port);
    }
	private void closeSlave(Socket slave) throws Exception {
        slave.close();
    }
	public void run(){
		try{
		socket = connectToSlave();
		requestMsg.setKey(key);
		requestMsg.setValue(value);
		requestMsg.sendMessage(socket);
		        
		KVMessage responseMsg = new KVMessage(socket.getInputStream());
		closeSlave(socket);
		socket = connectToSlave();
		KVMessage decis = new KVMessage(KVMessage.COMMIT);
		decis.sendMessage(socket);
	
		      
		KVMessage ack = new KVMessage(socket.getInputStream());
		closeSlave(socket);
		
		
		} catch(Exception e){
			
		}
	}
}
class getParallel implements Runnable{
	KVMessage request;
	String key;
	Socket socket;
	int port;
	String expectedValue;
	
	public getParallel( KVMessage request, String key, 
			int port, String expectedValue){
		this.request= request;
		this.key = key;
		this.port = port;
		this.expectedValue = expectedValue;
	}
	private Socket connectToSlave() throws Exception {
        return new Socket("localhost", port);
    }
	private void closeSlave(Socket slave) throws Exception {
        slave.close();
    }
	public void run(){
		KVMessage resp = null;
		try{
		  socket = connectToSlave();
	      KVMessage getReq = new KVMessage(KVMessage.GETREQ);
	      getReq.setKey(key);
		
	        getReq.sendMessage(socket);
	        
	        resp = new KVMessage(socket.getInputStream());
	        closeSlave(socket);
	        assertEquals(resp.getKey(), expectedValue);
		}catch(Exception e){
			
		}
	        
	}
	

}
