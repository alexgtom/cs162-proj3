package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.io.*;
public class ThreadPoolTest{
	
    private boolean job1 = false;
    static int count = 0;
    static int b = 0;
    
    public synchronized void addCount(){
    	count++;
    }
	@Test
	public void test1(){
		try{
			ThreadPool t = new ThreadPool(1);
			Runnable r = new Job1();
			t.addToQueue(r);
			while(!job1){
			}
            t.stop();
            assertEquals(true, job1);
		}catch(Exception e){
			//should not happen
			e.printStackTrace();
		}
		
	}
	@Test
	public void test2(){
		try{
			ThreadPool t = new ThreadPool(50);
			for (int i=0;i<50;i++){
				t.addToQueue(new Job2());
			}
			while(count!=50){
			}
            t.stop();
			assertEquals(count, 50);
		}catch(Exception e){
			//should not happen
			e.printStackTrace();
		}
		
	}
	@Test
	public void test3(){
		try{
			count = 0;
			ThreadPool t = new ThreadPool(50);
			for (int i=0;i<100;i++){
				t.addToQueue(new Job2());
			}
			while(count!=100){
			}
            t.stop();
			assertEquals(count, 100);
		}catch(Exception e){
			//should not happen
			e.printStackTrace();
		}
		
	}
	@Test
	public void test4(){
		try{
			count = 0;
			ThreadPool t = new ThreadPool(1);
			for (int i=0;i<100;i++){
				t.addToQueue(new Job2());
			}
			while(count!=100){
			}
            t.stop();
			assertEquals(count, 100);
		}catch(Exception e){
			//should not happen
			e.printStackTrace();
		}
	}
	@Test
	public void test5(){
		try{
			count = 0;
			ThreadPool t = new ThreadPool(100);
			for (int i=0;i<50;i++){
				t.addToQueue(new Job2());
			}
			while(count!=50){
			}
            t.stop();
			Thread.sleep(100);
			assertEquals(count, 50);
		}catch(Exception e){
			//should not happen
			e.printStackTrace();
		}
		
	}
	
	
	
	public class Job2 implements Runnable{
		public void run(){
			addCount();
		}
	}

	public class Job1 implements Runnable{
		public void run(){
			job1 = true;
		}
	}

	
	
}
