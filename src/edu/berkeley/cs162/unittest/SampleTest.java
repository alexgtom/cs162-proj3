/*
 * Example test cases
 */
package edu.berkeley.cs162.unittest;

import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.*;

public class SampleTest {
    @Test
    public void testEmptyCollection() {
        Collection collection = new ArrayList();
        assertTrue(collection.isEmpty());
        
        // Example of using mockito
        List mockedList = mock(List.class);
    }
}
