package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import java.io.File;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;

public class KVStoreTest {
    @Test
    public void simpleTest() throws KVException {
        KVStore store = new KVStore(); 
        store.put("A", "1");
        store.put("B", "2");
        store.put("C", "3");
        store.put("D", "4");
        store.put("E", "5");
        assertEquals(5, store.size());

        store.dumpToFile("KVStoreDump.txt");
        //I think if we check on the old one might not achieve the goal of testing.
        store.restoreFromFile("KVStoreDump.txt");
        assertEquals(5, store.size());
        
        assertEquals("1", store.get("A"));
        assertEquals("2", store.get("B"));
        assertEquals("3", store.get("C"));
        assertEquals("4", store.get("D"));
        assertEquals("5", store.get("E"));
        assertEquals(5, store.size());
        // delete KVStoreDump.txt
        File f = new File("KVStoreDump.txt");
        f.delete();
    }

    @Test
    public void bigTest() throws KVException { 
        KVStore store = new KVStore(); 
        int storeSize = 200;
        for(int i = 0; i < storeSize; i++) {
            store.put(i + "", i + "");
        }
        assertEquals(storeSize, store.size());
        store.dumpToFile("KVStoreDump.txt");
        for(int i = 0; i < storeSize; i++) {
            store.put(i + "", "invalid");
        }
        assertEquals(storeSize, store.size());
        store.restoreFromFile("KVStoreDump.txt");
        for(int i = 0; i < storeSize; i++) {
            assertEquals(i + "", store.get(i + ""));
        }
        assertEquals(storeSize, store.size());
        File f = new File("KVStoreDump.txt");
        f.delete();
    }

    @Test
    public void toXMLTest() throws KVException {
        KVStore store = new KVStore(); 

        store.put("A", "1");
        store.put("B", "2");
        store.put("C", "3");
        store.put("D", "4");
        store.put("E", "5");
        String s = store.toXML();
        assertTrue("XML missing item(s)", s.contains("A") && s.contains("2") && s.contains("C") && s.contains("3") && s.contains("E"));

        store.put("F","6");
        store.put("G","7");
        s = store.toXML();
        assertTrue("XML missing item(s)", s.contains("F") && s.contains("G") && s.contains("6") && s.contains("7"));
        assertTrue("XML missing item(s)", s.contains("A") && s.contains("2") && s.contains("C") && s.contains("3") && s.contains("E"));


    }
}
