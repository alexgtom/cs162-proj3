package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;

public class TPCMasterHandlerTest {

    private int tpcOpId = -1;

    private String getNextTpcOpId() {
        return "" + ++tpcOpId; 
    }
    private Socket connectToSlave() throws Exception {
        return new Socket("localhost", 50000);
    }

    private void closeSlave(Socket slave) throws Exception {
        slave.close();
    }

    private String strGen(int size) {
        byte[] arr = new byte[size];
        return new String(arr);
    }

    /*
     * Performs a TPC operation depending on the requestType
     */

    private void tpcOperation(String requestType, String key, String value, 
            String globalDecision, String localDecision, boolean debug, int numResendDecision) throws Exception {
        // send PREPARE 
        Socket slaveServer = connectToSlave();
        KVMessage tpcReq = new KVMessage(requestType);
        if(key != null)
            tpcReq.setKey(key);
        if(value != null)
            tpcReq.setValue(value);
        tpcReq.setTpcOpId(getNextTpcOpId());
        if (debug) {
            tpcReq.debugPrint();
        }
        tpcReq.sendMessage(slaveServer);


        // wait for RESPONSE
        KVMessage resp = new KVMessage(slaveServer.getInputStream());
        if (debug) {
            resp.debugPrint();
        }
        assertEquals(localDecision, resp.getMsgType());
        closeSlave(slaveServer);

        // send DECISION (COMMIT/ABORT)
        for(int i = 0; i < numResendDecision; i++) {
            slaveServer = connectToSlave();
            KVMessage decis = new KVMessage(globalDecision);
            decis.setTpcOpId(getNextTpcOpId());
            if (debug) {
                decis.debugPrint();
            }
            decis.sendMessage(slaveServer);
        }

        // wait for ACK 
        KVMessage ack = new KVMessage(slaveServer.getInputStream());
        if (debug) {
            ack.debugPrint();
        }
        assertEquals("ack", ack.getMsgType());
        closeSlave(slaveServer);

    }

    private void tpcOperation(String requestType, String key, String value, 
            String globalDecision, String localDecision, boolean debug) throws Exception {
        tpcOperation(requestType, key, value, globalDecision, localDecision, debug, 1);
    }
    /*
     * Performs a GETREQ and returns the KVMessage of that request
     */
    private KVMessage getOperation(String key, boolean debug) throws Exception {
        Socket slaveServer = connectToSlave();
        KVMessage getReq = new KVMessage(KVMessage.GETREQ);
        getReq.setKey(key);
        if (debug) {
            getReq.debugPrint();
        }
        getReq.sendMessage(slaveServer);

        KVMessage resp = new KVMessage(slaveServer.getInputStream());
        closeSlave(slaveServer);
        if (debug) {
            resp.debugPrint();
        }
        return resp;
    }

    private void ignoreOperation(boolean debug) throws Exception {
        Socket slaveServer = connectToSlave();
        KVMessage ignore = new KVMessage(KVMessage.IGNORENEXT);
        if (debug) {
            ignore.debugPrint();
        }
        ignore.sendMessage(slaveServer);

        KVMessage resp = new KVMessage(slaveServer.getInputStream());
        closeSlave(slaveServer);
        assertEquals(resp.getMessage(), "Success");
        assertEquals(resp.getMsgType(), KVMessage.RESP);
        if (debug) {
            resp.debugPrint();
        }
    }   

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void ignoreTest() throws Exception {
        ignoreOperation(false);
        tpcOperation(KVMessage.PUTREQ, "key", "value", KVMessage.ABORT, KVMessage.ABORT, false);
    }

    @Test
    public void badDeleteTest() throws Exception {
        tpcOperation(KVMessage.DELREQ, "key", null, KVMessage.ABORT, KVMessage.ABORT, false);
    }

    @Test
    public void getTestNoSuchKey() throws Exception {
        KVMessage resp = getOperation("superrandomkey", false);
        assertEquals(KVError.DOES_NOT_EXIST.toString(), resp.getMessage());
    }

    @Test
    public void getAndPutTest() throws Exception {
        tpcOperation(KVMessage.PUTREQ, "test", "value", KVMessage.COMMIT, KVMessage.READY, false);

        // try to get value 
        KVMessage resp = getOperation("test", false);
        assertEquals("test", resp.getKey());
        assertEquals("value", resp.getValue());

    }

    @Test
    public void getAndPutMultipleTimes() throws Exception {
        tpcOperation(KVMessage.PUTREQ, "test", "value", KVMessage.COMMIT, KVMessage.READY, false, 10);

        // try to get value 
        KVMessage resp = getOperation("test", false);
        assertEquals("test", resp.getKey());
        assertEquals("value", resp.getValue());
    }

    @Test
    public void getPutDelTest() throws Exception {
        tpcOperation(KVMessage.PUTREQ, "test", "value", KVMessage.COMMIT, KVMessage.READY, false);
        tpcOperation(KVMessage.DELREQ, "test", null, KVMessage.COMMIT, KVMessage.READY, false);
        KVMessage resp = getOperation("test", false);
        assertEquals(KVError.DOES_NOT_EXIST.toString(), resp.getMessage());
    }

    @Test
    public void getAndPutOverrideTest() throws Exception {
        tpcOperation(KVMessage.PUTREQ, "test", "value", KVMessage.COMMIT, KVMessage.READY, false);
        tpcOperation(KVMessage.PUTREQ, "test", "newvalue", KVMessage.COMMIT, KVMessage.READY, false);

        // try to get value 
        KVMessage resp = getOperation("test", false);
        assertEquals("test", resp.getKey());
        assertEquals("newvalue", resp.getValue());
    }

    @Test
    public void failTest() throws Exception {
        Socket slaveServer = connectToSlave();
        KVMessage tpcReq = new KVMessage(KVMessage.PUTREQ);
        tpcReq.setKey("key1");
        tpcReq.setValue("value1");
        tpcReq.setTpcOpId(getNextTpcOpId());
        tpcReq.sendMessage(slaveServer);

        KVMessage resp = new KVMessage(slaveServer.getInputStream());
        closeSlave(slaveServer);	

        slaveServer = connectToSlave();
        KVMessage reset = new KVMessage("reset");
        reset.sendMessage(slaveServer);
        closeSlave(slaveServer);

        slaveServer = connectToSlave();
        KVMessage decis = new KVMessage(KVMessage.COMMIT);
        decis.setTpcOpId(getNextTpcOpId());
        decis.sendMessage(slaveServer);

        KVMessage ack = new KVMessage(slaveServer.getInputStream());
        assertEquals("ack", ack.getMsgType());
        closeSlave(slaveServer);

        resp = getOperation("key1", false);
        assertEquals("key1", resp.getKey());	

    }

    @Test
    public void failPutTest() throws Exception {
        Socket slaveServer = connectToSlave();
        KVMessage tpcReq = new KVMessage(KVMessage.PUTTEST);
        tpcReq.setKey("key1");
        tpcReq.setValue("value1");
        tpcReq.setTpcOpId(getNextTpcOpId());
        tpcReq.sendMessage(slaveServer);
        exception.expect(KVException.class);

        KVMessage resp = new KVMessage(slaveServer.getInputStream());
        closeSlave(slaveServer);    

        slaveServer = connectToSlave();
        KVMessage reset = new KVMessage("reset");
        reset.sendMessage(slaveServer);
        closeSlave(slaveServer);

        slaveServer = connectToSlave();
        KVMessage decis = new KVMessage(KVMessage.COMMIT);
        decis.setTpcOpId(getNextTpcOpId());
        decis.sendMessage(slaveServer);

        KVMessage ack = new KVMessage(slaveServer.getInputStream());
        assertEquals("ack", ack.getMsgType());
        closeSlave(slaveServer);

        resp = getOperation("key1", false);
        assertEquals("key1", resp.getKey());    

    }

    @Test
    public void failDelTest() throws Exception {
        Socket slaveServer = connectToSlave();
        KVMessage tpcReq = new KVMessage(KVMessage.DELTEST);
        tpcReq.setKey("key1");
        tpcReq.setTpcOpId(getNextTpcOpId());
        tpcReq.sendMessage(slaveServer);
        exception.expect(KVException.class);

        KVMessage resp = new KVMessage(slaveServer.getInputStream());
        closeSlave(slaveServer);    

        slaveServer = connectToSlave();
        KVMessage reset = new KVMessage("reset");
        reset.sendMessage(slaveServer);
        closeSlave(slaveServer);

        slaveServer = connectToSlave();
        KVMessage decis = new KVMessage(KVMessage.COMMIT);
        decis.setTpcOpId(getNextTpcOpId());
        decis.sendMessage(slaveServer);

        KVMessage ack = new KVMessage(slaveServer.getInputStream());
        assertEquals("ack", ack.getMsgType());
        closeSlave(slaveServer);

        resp = getOperation("key1", false);
        assertEquals("key1", resp.getKey());    

    }


}
