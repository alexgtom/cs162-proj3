package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;

import java.lang.Integer;

public class KVCacheTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();
    
    @Test
    public void putAndGetTest() throws KVException {
        KVCache cache = new KVCache(100, 10);
        cache.put("k1", "1");
        cache.put("k2", "2");
        assertEquals("1", cache.get("k1"));
        assertEquals("2", cache.get("k2"));
    }

    @Test
    public void toXMLTest() throws KVException {
        KVCache cache = new KVCache(1, 2);
        cache.put("k1", "1");
        cache.put("k2", "2");

        String xml = "";
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVCache>";
        xml += "  <Set Id=\"0\">";
        xml += "    <CacheEntry isReferenced=\"false\" isValid=\"true\">";
        xml += "      <Key>k1</Key>";
        xml += "      <Value>1</Value>";
        xml += "    </CacheEntry>";
        xml += "    <CacheEntry isReferenced=\"false\" isValid=\"true\">";
        xml += "      <Key>k2</Key>";
        xml += "      <Value>2</Value>";
        xml += "    </CacheEntry>";
        xml += "  </Set>";
        xml += "</KVCache>";

        assertEquals(xml.replaceAll("\\s", ""), 
                cache.toXML().replaceAll("\\s", ""));
    }

    @Test
    public void toXML2Test() throws KVException {
        KVCache cache = new KVCache(2, 1);
        cache.put("k1", "1");
        cache.put("k2", "2");

        String xml = "";
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVCache>";
        xml += "  <Set Id=\"0\">";
        xml += "    <CacheEntry isReferenced=\"false\" isValid=\"true\">";
        xml += "      <Key>k1</Key>";
        xml += "      <Value>1</Value>";
        xml += "    </CacheEntry>";
        xml += "  </Set>";
        xml += "  <Set Id=\"1\">";
        xml += "    <CacheEntry isReferenced=\"false\" isValid=\"true\">";
        xml += "      <Key>k2</Key>";
        xml += "      <Value>2</Value>";
        xml += "    </CacheEntry>";
        xml += "  </Set>";
        xml += "</KVCache>";

        assertEquals(xml.replaceAll("\\s", ""), 
                cache.toXML().replaceAll("\\s", ""));
    }
}
