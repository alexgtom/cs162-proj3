package edu.berkeley.cs162.unittest;

/*
 * This file runs all test suites
 */
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import junit.framework.JUnit4TestAdapter;
import edu.berkeley.cs162.*;

@RunWith(Suite.class)
// specify the test suites to run
@SuiteClasses({ 
    SampleTest.class,
    KVMessageTest.class,
    RangeMapTest.class,
    KVStoreTest.class,
    CacheSetTest.class,
    KVCacheTest.class,
    ThreadPoolTest.class,
    KVServerTest.class,
    KVServer2Test.class,
    TPCMasterHandlerTest.class,
    KVClientTest.class,
    TPCLogTest.class,
    TPCMasterTest.class,
    ParallelTest.class,
}) 

public class AllTests {
//    private static long tempStoreDelay;
//    private static long tempCacheDelay;
//
//    @BeforeClass 
//    public static void setUpClass() throws Exception {      
//        tempStoreDelay = AutoGrader.STORE_DELAY;
//        tempCacheDelay = AutoGrader.CACHE_DELAY;
//
//        AutoGrader.STORE_DELAY = 0; 
//        AutoGrader.CACHE_DELAY = 0; 
//
//    }
//
//    @AfterClass 
//    public static void tearDownClass() { 
//        AutoGrader.STORE_DELAY = tempStoreDelay; 
//        AutoGrader.CACHE_DELAY = tempCacheDelay; 
//    }

    public static junit.framework.Test suite() 
    {
       return new JUnit4TestAdapter(AllTests.class);
    }

    public static void main(final String[] args) {
        junit.textui.TestRunner.run (suite());
    }

}
