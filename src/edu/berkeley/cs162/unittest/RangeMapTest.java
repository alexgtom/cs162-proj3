package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;

public class RangeMapTest {
    RangeMap<String> map = null; 
    
    @Before
    public void setUp() {
        map = new RangeMap<String>();
    }

    @Test
    public void simpleTest() {
        map.put((long) Math.pow(2, 23), "S1");
        map.put((long) Math.pow(2, 35), "S2");
        map.put((long) Math.pow(2, 55), "S3");
        map.put((long) Math.pow(2, 16), "S4");
        assertEquals(4, map.size());

        assertEquals("S1", map.get((long) Math.pow(2, 16) + 1));
        assertEquals("S1", map.get((long) Math.pow(2, 18)));
        assertEquals("S1", map.get((long) Math.pow(2, 23)));

        assertEquals("S2", map.get((long) Math.pow(2, 23) + 1));
        assertEquals("S2", map.get((long) Math.pow(2, 24)));
        assertEquals("S2", map.get((long) Math.pow(2, 25)));

        assertEquals("S3", map.get((long) Math.pow(2, 35) + 1));
        assertEquals("S3", map.get((long) Math.pow(2, 45)));
        assertEquals("S3", map.get((long) Math.pow(2, 55)));

        assertEquals("S4", map.get((long) Math.pow(2, 55) + 1));
        assertEquals("S4", map.get((long) Math.pow(2, 60)));
        assertEquals("S4", map.get((long) Math.pow(2, 64) - 1));
        assertEquals("S4", map.get((long) 0));
        assertEquals("S4", map.get((long) Math.pow(2, 10)));
        assertEquals("S4", map.get((long) Math.pow(2, 16)));

        assertEquals("S2", map.getSuccessor((long) Math.pow(2, 16) + 1));
        assertEquals("S2", map.getSuccessor((long) Math.pow(2, 18)));
        assertEquals("S2", map.getSuccessor((long) Math.pow(2, 23)));

        assertEquals("S3", map.getSuccessor((long) Math.pow(2, 23) + 1));
        assertEquals("S3", map.getSuccessor((long) Math.pow(2, 24)));
        assertEquals("S3", map.getSuccessor((long) Math.pow(2, 25)));

        assertEquals("S4", map.getSuccessor((long) Math.pow(2, 35) + 1));
        assertEquals("S4", map.getSuccessor((long) Math.pow(2, 45)));
        assertEquals("S4", map.getSuccessor((long) Math.pow(2, 55)));

        assertEquals("S1", map.getSuccessor((long) Math.pow(2, 55) + 1));
        assertEquals("S1", map.getSuccessor((long) Math.pow(2, 60)));
        assertEquals("S1", map.getSuccessor((long) Math.pow(2, 64) - 1));
        assertEquals("S1", map.getSuccessor((long) 0));
        assertEquals("S1", map.getSuccessor((long) Math.pow(2, 10)));
        assertEquals("S1", map.getSuccessor((long) Math.pow(2, 16)));
    }

    @Test
    public void simple2Test() {
        map.put((long) Math.pow(2, 23), "S1");

        assertEquals("S1", map.get((long) Math.pow(2, 16) + 1));
        assertEquals("S1", map.get((long) Math.pow(2, 18)));
        assertEquals("S1", map.get((long) Math.pow(2, 23)));
        assertEquals("S1", map.get((long) 0));
        assertEquals("S1", map.getSuccessor((long) Math.pow(2, 16) + 1));
        assertEquals("S1", map.getSuccessor((long) Math.pow(2, 18)));
        assertEquals("S1", map.getSuccessor((long) Math.pow(2, 23)));
        assertEquals("S1", map.getSuccessor((long) 0));
    }

    @Test
    public void simple3Test() {
        long first = -664818820L;
        long second = -1817869604L;

        map.put(first, "S1");
        map.put(second, "S2");
        assertEquals("S1", map.get(first));
        assertEquals("S2", map.get(second));

        assertEquals("S1", map.getSuccessor(second));
        assertEquals("S2", map.getSuccessor(first));
    }

    @Test
    public void bigTest() {
        for(int offset = 0; offset < 1024; offset += 4)
            for(int numNodes = 0; numNodes < 64; numNodes++) {
                map =  new RangeMap<String>();
                // insert nodes
                for(int i = 0; i < numNodes; i++)
                    map.put((long) Math.pow(2, i % 64) + offset, "S" + numNodes);
                
                // get nodes
                for(int i = 0; i < numNodes; i++)
                    assertEquals("S" + numNodes, map.get((long) Math.pow(2, i % 64) + 1 + offset));
            }
    }
}
