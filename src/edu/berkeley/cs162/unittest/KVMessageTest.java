package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;
import java.util.regex.Matcher;

public class KVMessageTest {
    /**
     * helper functions
     */
    public String createDummyString(int length) {
        byte [] arr = new byte[length];
        return new String(arr);
    }

    /***************************************************************************
     * Errors
     **************************************************************************/
    @Rule
    public ExpectedException exception = ExpectedException.none();
    
    @Test
    public void error1Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());
        new KVMessage("Invalid msg type");
    }

    @Test
    public void error2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());
        new KVMessage("Invalid msg type", "message");
    }

    @Test
    public void error3Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.XML_ERROR.toString());
        new KVMessage(new ByteArrayInputStream("invalid xml".getBytes()));
    }
    
    //@Test
    //public void error4Test() throws KVException {
    //    exception.expect(KVException.class);
    //    exception.expectMessage(KVError.NETWORK_ERROR_RECEIVE_DATA.toString());
    //    String xml = "";
    //    new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    //}

    @Test
    public void keyEmptyStringTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());
        String xml = "";
        KVMessage m = new KVMessage("getreq");
        m.setKey("");
    }

    @Test
    public void valueEmptyStringTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_VALUE.toString());
        String xml = "";
        KVMessage m = new KVMessage("getreq");
        m.setValue("");
    }

    @Test
    public void keySetNullTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());
        String xml = "";
        KVMessage m = new KVMessage("getreq");
        m.setKey(null);
    }

    @Test
    public void valueSetNullTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_VALUE.toString());
        String xml = "";
        KVMessage m = new KVMessage("getreq");
        m.setValue(null);
    }

    @Test
    public void missingTypeTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing key
        String xml = 
           "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
           "<KVMessage>" + 
           "<Key>key</Key>" + 
           "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void zeroLengthValuePutTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_VALUE.toString());

        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"putreq\">" + 
            "<Key>key</Key>" + 
            "<Value></Value>" + 
            "<TPCOpId>0</TPCOpId>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

     
    @Test
    public void zeroLengthKeyPutTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());

        // missing key
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"putreq\">" + 
            "<Key></Key>" + 
            "<Value>value</Value>" + 
            "<TPCOpId>0</TPCOpId>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }
    
    @Test
    public void zeroLengthKeyPut2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());

        // missing key
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"putreq\">" + 
            "<Key></Key>" + 
            "<Value>value</Value>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void zeroLengthKeyGetTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());

        // missing key
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"getreq\">" + 
            "<Key></Key>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void zeroLengthKeyDelTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());

        // missing key
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"delreq\">" + 
            "<Key></Key>" + 
            "<TPCOpId>0</TPCOpId>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }


    @Test
    public void zeroLengthKeyDel2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());

        // missing key
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"delreq\">" + 
            "<Key></Key>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }


    @Test
    public void zeroLengthValueRespTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_VALUE.toString());

        // missing key
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            "<Key>key</Key>" + 
            "<Value></Value>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

     
    @Test
    public void zeroLengthKeyRespTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.INVALID_KEY.toString());

        // missing key
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            "<Key></Key>" + 
            "<Value>value</Value>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }
    @Test
    public void getRequestErrorTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing key
        String xml = 
           "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
           "<KVMessage type=\"getreq\">" + 
           // "<Key>key</Key>" + 
           "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

     
    @Test
    public void putValueRequestMissingKeyErrorTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing key
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"putreq\">" + 
            //"<Key>key</Key>" + 
            "<Value>value</Value>" + 
            "<TPCOpId>0</TPCOpId>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void putValueRequestMissingValueErrorTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"putreq\">" + 
            "<Key>key</Key>" + 
            //"<Value>value</Value>" + 
            "<TPCOpId>0</TPCOpId>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void deleteValueRequestMissingKeyErrorTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"delreq\">" + 
            //"<Key>key</Key>" + 
            "<TPCOpId>0</TPCOpId>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void successfulGetResponseMissingKey() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            //"<Key>key</Key>" + 
            "<Value>value</Value>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void successfulGetResponseMissingValue() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            "<Key>key</Key>" + 
            //"<Value>value</Value>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }
    

    @Test
    public void successfulPutResponseMissingMessage() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            //"<Message>Success</Message>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void successfulDeleteResponseMissingMessage() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            //"<Message>Success</Message>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }



    @Test
    public void unsuccessfulGetPutDeleteResponseMissingMessage() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            //"<Message>Error Message</Message>" + 
            "<TPCOpId>0</TPCOpId>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void invalidKVMessageType1Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"invalid type\">" + 
            //"<Message>Error Message</Message>" + 
            "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void invalidKVMessageType2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        KVMessage m = new KVMessage("invalid");
    }

    @Test
    public void invalidKVMessageNothingSetTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        // missing value
        KVMessage m = new KVMessage("getreq");
        m.toXML();
    }


    @Test
    public void invalidReadyTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        KVMessage m = new KVMessage("ready");
        m.toXML();
    }

    @Test
    public void invalidReady2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        String xml = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"ready\">";
        //xml += "<TPCOpId>2PC Operation ID</TPCOpId>";
        xml += "</KVMessage>";

        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void invalidAbortTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        KVMessage m = new KVMessage("abort");
        m.setMessage("Error Message");
        m.toXML();
    }
    
    @Test
    public void invalidAbort2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        String xml = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"abort\">";
        xml += "<Message>Error Message</Message>";
        //xml += "<TPCOpId>2PC Operation ID</TPCOpId>";
        xml += "</KVMessage>";

        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void invalidDecisionTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        KVMessage m = new KVMessage("abort");
        m.toXML();
    }
    
    @Test
    public void invalidDecision2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        String xml = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"abort\">";
        //xml += "<TPCOpId>2PC Operation ID</TPCOpId>";
        xml += "</KVMessage>";

        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void invalidAckTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        KVMessage m = new KVMessage("ack");
        m.toXML();
    }

    @Test
    public void invalidAck2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        String xml = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"ack\">";
        //xml += "<TPCOpId>2PC Operation ID</TPCOpId>";
        xml += "</KVMessage>";

        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void invalidRegisterTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        KVMessage m = new KVMessage("register");
        m.toXML();
    }

    @Test
    public void invalidRegister2Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        String xml = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"register\">";
        //xml += "<Message>SlaveServerID@HostName:Port</Message>";
        xml += "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void invalidRegister3Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.REGISTRATION_ERROR.toString());

        String xml = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"register\">";
        xml += "<Message>@:</Message>";
        xml += "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }
    
    @Test
    public void invalidRegister4Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.REGISTRATION_ERROR.toString());

        String xml = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"register\">";
        xml += "<Message>SlaverServerId@HostName</Message>";
        xml += "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    @Test
    public void registerMessageValidationTest() throws KVException {
        KVMessage m = new KVMessage("register");
        assertTrue(m.isValidateRegisterMessage("SlaveServerID@HostName:Port"));
        assertTrue(m.isValidateRegisterMessage("D@H:t"));
        assertFalse(m.isValidateRegisterMessage("@:"));
        assertFalse(m.isValidateRegisterMessage("SlaverServer@HostName"));
        assertFalse(m.isValidateRegisterMessage("SlaverServer:port"));
        //assertFalse(m.isValidateRegisterMessage("SlaverServer @ Hostnmae : port"));
    }

    @Test
    public void slaveInfoMatcherTest() {
        Matcher m = KVMessage.slaveInfoMatcher("SlaveServerID@HostName:Port");
        m.find();
        assertEquals("SlaveServerID", m.group(1));
        assertEquals("HostName", m.group(2));
        assertEquals("Port", m.group(3));
    }

    @Test
    public void slaveInfoMatcher2Test() {
        Matcher m = KVMessage.slaveInfoMatcher("12312@HostName:Port");
        m.find();
        assertEquals("12312", m.group(1));
        assertEquals("HostName", m.group(2));
        assertEquals("Port", m.group(3));
    }

    @Test
    public void slaveInfoMatcher3Test() {
        Matcher m = KVMessage.slaveInfoMatcher("-12312@HostName:Port");
        m.find();
        assertEquals("-12312", m.group(1));
        assertEquals("HostName", m.group(2));
        assertEquals("Port", m.group(3));
    }

    @Test
    public void invalidRegister5Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.REGISTRATION_ERROR.toString());

        String xml = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"register\">";
        xml += "<Message>SlaverServerId:port</Message>";
        xml += "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }
    
    @Test
    public void invalidRegistrationAckTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        KVMessage m = new KVMessage("register");
        m.toXML();
    }

    @Test
    public void invalidRegistration2AckTest() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.MSG_FORMAT_INCORRECT.toString());

        String xml = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"register\">";
        //xml += "<Message>SlaveServerID@HostName:Port</Message>";
        xml += "</KVMessage>";
        new KVMessage(new ByteArrayInputStream(xml.getBytes()));
    }

    /***************************************************************************
     * Valid messages
     **************************************************************************/
    
    @Test 
    public void getValueRequestTest() throws KVException {
        // Get Value Request:
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"getreq\">" + 
            "<Key>key</Key>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        assertEquals(KVMessage.GETREQ, m.getMsgType());
        assertEquals("key", m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }
    
    @Test 
    public void putValueRequestTest() throws KVException {
        // Put Value Request:
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"putreq\">" + 
            "<Key>key</Key>" + 
            "<Value>value</Value>" + 
            "<TPCOpId>0</TPCOpId>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        assertEquals(KVMessage.PUTREQ, m.getMsgType());
        assertEquals("key", m.getKey());
        assertEquals("value", m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals("0", m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }
    
    @Test 
    public void putValueRequest2Test() throws KVException {
        // Put Value Request:
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"putreq\">" + 
            "<Key>key</Key>" + 
            "<Value>value</Value>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        assertEquals(KVMessage.PUTREQ, m.getMsgType());
        assertEquals("key", m.getKey());
        assertEquals("value", m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }
    
    @Test 
    public void deleteValueRequestTest() throws KVException {
        // Delete Value Request:
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"delreq\">" + 
            "<Key>key</Key>" + 
            "<TPCOpId>0</TPCOpId>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        assertEquals(KVMessage.DELREQ, m.getMsgType());
        assertEquals("key", m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals("0", m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }
    
    @Test 
    public void deleteValueRequest2Test() throws KVException {
        // Delete Value Request:
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"delreq\">" + 
            "<Key>key</Key>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        assertEquals(KVMessage.DELREQ, m.getMsgType());
        assertEquals("key", m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }
    
    @Test 
    public void successfulGetResponseTest() throws KVException {
        // Successful Get Response:
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            "<Key>key</Key>" + 
            "<Value>value</Value>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        assertEquals(KVMessage.RESP, m.getMsgType());
        assertEquals("key", m.getKey());
        assertEquals("value", m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }
    
    @Test 
    public void successfulPutResponseTest() throws KVException {
        // Successful Put Response:
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            "<Message>Success</Message>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        assertEquals(KVMessage.RESP, m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(KVError.SUCCESS.toString(), m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }
    
    @Test 
    public void successfulDeleteResponseTest() throws KVException {
        // Successful Delete Response:
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            "<Message>Success</Message>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        assertEquals(KVMessage.RESP, m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(KVError.SUCCESS.toString(), m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }
    
    @Test 
    public void unsuccessfulGetPutDeleteResponseTest() throws KVException {
        // Unsuccessful Get/Put/Delete Response:
        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"resp\">" + 
            "<Message>Error Message</Message>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        assertEquals(KVMessage.RESP, m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(null, m.getTpcOpId());
        assertEquals("Error Message", m.getMessage());
        assertEquals(xml, m.toXML());
    }

    @Test 
    public void unsuccessfulGetPutDelete2ResponseTest() throws KVException {
        // Unsuccessful Get/Put/Delete Response:
        KVMessage m2 = new KVMessage("resp", KVError.MSG_FORMAT_INCORRECT);
        assertEquals(KVMessage.RESP, m2.getMsgType());
        assertEquals(null, m2.getKey());
        assertEquals(null, m2.getValue());
        assertEquals(null, m2.getTpcOpId());
        assertEquals(KVError.MSG_FORMAT_INCORRECT.toString(), m2.getMessage());
    }

    @Test
    public void oversizeKey1Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_KEY.toString());
        KVMessage m = new KVMessage(KVMessage.RESP);
        m.setKey(createDummyString(KVMessage.MAX_KEY_SIZE + 1));
    }

    @Test
    public void oversizeKey2Test() throws KVException {
        KVMessage m = new KVMessage(KVMessage.RESP);
        m.setKey(createDummyString(KVMessage.MAX_KEY_SIZE));
        assertEquals(KVMessage.MAX_KEY_SIZE, m.getKey().length());
    }
    
    @Test
    public void oversizeValue1Test() throws KVException {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.OVERSIZED_VALUE.toString());
        KVMessage m = new KVMessage(KVMessage.RESP);
        m.setValue(createDummyString(KVMessage.MAX_VALUE_SIZE + 1));
    }

    @Test
    public void oversizeValue2Test() throws KVException {
        KVMessage m = new KVMessage(KVMessage.RESP);
        m.setValue(createDummyString(KVMessage.MAX_VALUE_SIZE));
        assertEquals(KVMessage.MAX_VALUE_SIZE, m.getValue().length());
    }

    @Test
    public void readyTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"ready\">";
        xml += "<TPCOpId>0</TPCOpId>";
        xml += "</KVMessage>";

        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("ready", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals("0", m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }
    
    @Test
    public void abortErrorTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"abort\">";
        xml += "<Message>Error Message</Message>";
        xml += "<TPCOpId>0</TPCOpId>";
        xml += "</KVMessage>";

        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("abort", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals("Error Message", m.getMessage());
        assertEquals("0", m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }

    @Test
    public void decisionAbortTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"abort\">";
        xml += "<TPCOpId>0</TPCOpId>";
        xml += "</KVMessage>";

        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("abort", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals("0", m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }

    @Test
    public void decisionCommitTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"commit\">";
        xml += "<TPCOpId>0</TPCOpId>";
        xml += "</KVMessage>";

        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("commit", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals("0", m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }

    @Test
    public void ackTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"ack\">";
        xml += "<TPCOpId>0</TPCOpId>";
        xml += "</KVMessage>";

        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("ack", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals("0", m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }

    @Test
    public void registerTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"register\">";
        xml += "<Message>SlaveServerID@HostName:Port</Message>";
        xml += "</KVMessage>";


        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("register", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals("SlaveServerID@HostName:Port", m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }


    @Test
    public void registrationAckTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"resp\">";
        xml += "<Message>Successfully registered SlaveServerID@HostName:Port</Message>";
        xml += "</KVMessage>";

        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("resp", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals("Successfully registered SlaveServerID@HostName:Port", m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }

    @Test
    public void multipleErrorMessagesTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"resp\">";
        xml += "<Message>@SlaveServerID1:=ErrorMessage1\n@SlaveServerID2:=ErrorMessage2</Message>";
        xml += "</KVMessage>";

        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("resp", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals("@SlaveServerID1:=ErrorMessage1\n@SlaveServerID2:=ErrorMessage2", m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }


    @Test
    public void ignoreNextTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"ignoreNext\"/>";

        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("ignoreNext", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals(null, m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }


    @Test
    public void ignoreNextAckTest() throws KVException {
        String xml = "";

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        xml += "<KVMessage type=\"resp\">";
        xml += "<Message>Success</Message>";
        xml += "</KVMessage>";

        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));
        
        assertEquals("resp", m.getMsgType());
        assertEquals(null, m.getKey());
        assertEquals(null, m.getValue());
        assertEquals("Success", m.getMessage());
        assertEquals(null, m.getTpcOpId());
        assertEquals(xml, m.toXML());
    }

    @Ignore
    @Test
    public void sendMessageTest() throws Exception{

		KVClient kc = new KVClient("localhost", 8080);
        Socket sock = new Socket("localhost", 8080);

        String xml = 
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
            "<KVMessage type=\"getreq\">" + 
            "<Key>key</Key>" + 
            "</KVMessage>";
        KVMessage m = new KVMessage(new ByteArrayInputStream(xml.getBytes()));

        m.sendMessage(sock);
        
        while (sock.getInputStream().available() == 0) {
            
        }
        byte[] a = new byte[sock.getInputStream().available()];
        sock.getInputStream().read(a);
        System.out.println(new String(a));

        //KVMessage m2 = new KVMessage(sock.getInputStream());
        //assertEquals(xml, m2.toXML());
        //assertEquals(KVMessage.GETREQ, m2.getMsgType());
        //assertEquals("key", m2.getKey());
        //assertEquals(null, m2.getValue());
        //assertEquals(null, m2.getMessage());
    }    
}
