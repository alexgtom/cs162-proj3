package edu.berkeley.cs162.unittest;

import org.junit.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import edu.berkeley.cs162.*;

import java.util.*;
import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;
import java.util.ArrayList;

public class TPCLogTest {
    
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private TPCLog TPClog;
    
    @Test
    public void TestAppendAndFlush() throws Exception {
        KVServer kvserver = new KVServer(10, 10);
        TPClog = new TPCLog("1@1", kvserver);
        KVMessage message = new KVMessage(KVMessage.PUTREQ);
        message.setKey("hello");
        message.setValue("world");
        KVMessage commit = new KVMessage(KVMessage.COMMIT);
        TPClog.appendAndFlush(message);
        TPClog.appendAndFlush(commit);
        ArrayList<KVMessage> entries = null;
        entries = TPClog.getEntries();
        assertEquals(entries.get(0), message);
        assertEquals(entries.get(1), commit);
    }
    
    @Test
    public void TestPutCommit() throws Exception {
        KVServer kvserver = new KVServer(10, 10);
        TPClog = new TPCLog("1@2", kvserver);
        KVMessage message = new KVMessage(KVMessage.PUTREQ);
        message.setKey("hello");
        message.setValue("world");
        KVMessage commit = new KVMessage(KVMessage.COMMIT);
        TPClog.appendAndFlush(message);
        TPClog.appendAndFlush(commit);
        ArrayList<KVMessage> entries = null;
        entries = TPClog.getEntries();
        TPClog.rebuildKeyServer();
        assertEquals(kvserver.get("hello"), "world");
    }
    
    @Test
    public void TestPutAbort() throws Exception {
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        
        KVServer kvserver = new KVServer(10, 10);
        TPClog = new TPCLog("1@3", kvserver);
        KVMessage message = new KVMessage(KVMessage.PUTREQ);
        message.setKey("hello");
        message.setValue("world");
        KVMessage commit = new KVMessage(KVMessage.ABORT);
        TPClog.appendAndFlush(message);
        TPClog.appendAndFlush(commit);
        ArrayList<KVMessage> entries = null;
        entries = TPClog.getEntries();
        TPClog.rebuildKeyServer();
        kvserver.get("hello");
        Assert.fail("put operation was not aborted!");
    }

    @Test
	public void TestPutCommitDelCommitPutCommit() throws Exception{
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        
		KVServer kvserver = new KVServer(10, 10);
		TPClog = new TPCLog("1@4", kvserver);
		KVMessage message = new KVMessage(KVMessage.PUTREQ);
		message.setKey("hello");
		message.setValue("world");
		KVMessage commit = new KVMessage(KVMessage.COMMIT);
		TPClog.appendAndFlush(message);
		TPClog.appendAndFlush(commit);
		KVMessage message2 = new KVMessage(KVMessage.DELREQ);
		message2.setKey("hello");
		KVMessage commit2 = new KVMessage(KVMessage.COMMIT);
		TPClog.appendAndFlush(message2);
		TPClog.appendAndFlush(commit2);
		KVMessage message3 = new KVMessage(KVMessage.PUTREQ);
		message3.setKey("hello2");
		message3.setValue("world2");
		KVMessage commit3 = new KVMessage(KVMessage.COMMIT);
		TPClog.appendAndFlush(message3);
		TPClog.appendAndFlush(commit3);
		TPClog.rebuildKeyServer();
        
		assertEquals(kvserver.get("hello2"), "world2");
        kvserver.get("hello");
        Assert.fail("Key hello was not deleted!");
	}
    
    @Test
	public void TestPutCommitPut() throws Exception{
        exception.expect(KVException.class);
        exception.expectMessage(KVError.DOES_NOT_EXIST.toString());
        
		KVServer kvserver = new KVServer(10, 10);
		TPClog = new TPCLog("1@5", kvserver);
		KVMessage message = new KVMessage(KVMessage.PUTREQ);
		message.setKey("hello");
		message.setValue("world");
		KVMessage commit = new KVMessage(KVMessage.COMMIT);
		TPClog.appendAndFlush(message);
		TPClog.appendAndFlush(commit);
		KVMessage message2 = new KVMessage(KVMessage.PUTREQ);
		message2.setKey("hello2");
		message2.setValue("world2");
		TPClog.appendAndFlush(message2);
		TPClog.rebuildKeyServer();
        assertTrue(TPClog.hasInterruptedTpcOperation());
        KVMessage message3 = TPClog.getInterruptedTpcOperation();
        assertEquals(message3.getMsgType(), KVMessage.PUTREQ);
        assertEquals(message3.getKey(), "hello2");
        assertEquals(message3.getValue(), "world2");
        assertEquals(TPClog.getInterruptedTpcOperation(), null);
        
		assertEquals(kvserver.get("hello"), "world");
        kvserver.get("hello2");
        Assert.fail("Key hello was put into data store!");
	}
}
