/**
 * XML Parsing library for the key-value store
 *
 * @author Mosharaf Chowdhury (http://www.mosharaf.com)
 * @author Prashanth Mohan (http://www.cs.berkeley.edu/~prmohan)
 *
 * Copyright (c) 2012, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.io.FilterInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.io.OutputStream;
import java.net.Socket;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import edu.berkeley.cs162.*;
import java.io.IOException;
import org.xml.sax.SAXException;
import java.lang.IllegalArgumentException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.net.SocketTimeoutException;


/**
 * This is the object that is used to generate messages the XML based messages
 * for communication between clients and servers.
 */
public class KVMessage implements Serializable {

    private static final long serialVersionUID = 6473128480951955693L;

	private String msgType = null;
	private String key = null;
	private String value = null;
	private String message = null;
    private String tpcOpId = null;    
    
    public static final String GETREQ = "getreq";
    public static final String PUTREQ = "putreq";
    public static final String PUTTEST = "puttest";
    public static final String DELREQ = "delreq";
    public static final String DELTEST = "deltest";
    public static final String RESP = "resp";
    public static final String READY = "ready";
    public static final String ABORT = "abort";
    public static final String COMMIT = "commit";
    public static final String ACK = "ack";
    public static final String REGISTER = "register";
    public static final String IGNORENEXT = "ignoreNext";

    public static final String SLAVE_INFO_REGEX = "^(.+)@(.+):(.+)$";
    
    public static final int MAX_KEY_SIZE = 256; // in bytes
    public static final int MAX_VALUE_SIZE = 256 * 1024; // in bytes
    
	public final String getKey() {
		return key;
	}
    
	public final void setKey(String key) throws KVException {
		this.key = key;
        validateKey();
	}
    
	public final String getValue() {
		return value;
	}
    
	public final void setValue(String value) throws KVException {
		this.value = value;
        validateValue();
	}

	public final String getMessage() {
		return message;
	}
    
	public final void setMessage(String message) {
		this.message = message;
	}
    
	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
    
	public String getTpcOpId() {
		return tpcOpId;
	}

	public void setTpcOpId(String tpcOpId) {
		this.tpcOpId = tpcOpId;
	}

	/* Solution from http://weblogs.java.net/blog/kohsuke/archive/2005/07/socket_xml_pitf.html */
	private class NoCloseInputStream extends FilterInputStream {
	    public NoCloseInputStream(InputStream in) {
	        super(in);
	    }
	    
	    public void close() {} // ignore close

	}
	
	/***
	 *
	 * @param msgType
	 * @throws KVException of type "resp" with message "Message format incorrect" if msgType is unknown
	 */
    public KVMessage(String msgType) throws KVException {
        this.msgType = msgType;
        validateMessageType();    
	}
	
	public KVMessage(String msgType, String message) throws KVException {
        this(msgType);
        setMessage(message);
	}
	
	public KVMessage(String msgType, KVError message) throws KVException {
        this(msgType, message.toString());
	}

    /***
     * Parse KVMessage from incoming network connection
     * @param sock
     * @throws KVException if there is an error in parsing the message.
     * The exception should be of type "resp and message should be :
     * a. "XML Error: Received unparseable message" - if the received message is
     *                                                not valid XML.
     * b. "Network Error: Could not receive data" - if there is a network error
     *                                              causing an incomplete
     *                                              parsing of the message.
     * c. "Message format incorrect" - if there message does not conform to the
     *                                 required specifications. Examples include
     *                                 incorrect message type.
     */
	public KVMessage(InputStream input) throws KVException {
        parseInputStream(input);
	}


	/**
	 * 
	 * @param sock Socket to receive from
	 * @throws KVException if there is an error in parsing the message. The exception should be of type "resp and message should be :
	 * a. "XML Error: Received unparseable message" - if the received message is not valid XML.
	 * b. "Network Error: Could not receive data" - if there is a network error causing an incomplete parsing of the message.
	 * c. "Message format incorrect" - if there message does not conform to the required specifications. Examples include incorrect message type. 
	 */
    public KVMessage(Socket sock) throws KVException {
        this(sock, 0);
    }

	/**
	 * 
	 * @param sock Socket to receive from
	 * @param timeout Give up after timeout milliseconds
	 * @throws KVException if there is an error in parsing the message. The exception should be of type "resp and message should be :
	 * a. "XML Error: Received unparseable message" - if the received message is not valid XML.
	 * b. "Network Error: Could not receive data" - if there is a network error causing an incomplete parsing of the message.
	 * c. "Message format incorrect" - if there message does not conform to the required specifications. Examples include incorrect message type. 
	 */
    public KVMessage(Socket sock, int timeout) throws KVException {
        try {
            sock.setSoTimeout(timeout);
            parseInputStream(sock.getInputStream());
        } catch (SocketTimeoutException e) {
            throw new KVException(RESP, KVError.TIMEOUT);
        } catch (Exception e) {
            throw new KVException(RESP, e.getMessage());
        }
    }

    private void parseInputStream(InputStream input) throws KVException {
        DocumentBuilderFactory builderFactory = null;
        DocumentBuilder builder = null;
        Document doc = null;
        
        /*
         * Set variables
         */
        try {
            builderFactory = DocumentBuilderFactory.newInstance();
            builder = builderFactory.newDocumentBuilder();
            builder.setErrorHandler(null);
        } catch (ParserConfigurationException e) {
            throw new KVException(RESP, e.getMessage());
        }
        
        try {
            doc = builder.parse(new NoCloseInputStream(input));
        } catch (IOException e) {
            throw new KVException(RESP, KVError.NETWORK_ERROR_RECEIVE_DATA);
        } catch (SAXException e) {
            throw new KVException(RESP, KVError.XML_ERROR);
        } catch (IllegalArgumentException e) {
            throw new KVException(RESP, e.getMessage());
        }
        
        /*
         * Parse input
         */
        // try to set type, if we cant set it, throw an error
        try {
            this.msgType = getDocType(doc);
        } catch (IOException e) {
            throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
        }

        // set key
        try {
            setKey(getElementValue(doc, "Key"));
        } catch (IOException e) {
        }
        
        // set value
        try {
            setValue(getElementValue(doc, "Value"));
        } catch (IOException e) {
        }
        
        // set message
        try {
            setMessage(getElementValue(doc, "Message"));
        } catch (IOException e) {
        }

        // set tpcOpId
        try {
            setTpcOpId(getElementValue(doc, "TPCOpId"));
        } catch (IOException e) {
        }

        /*
         * validate message fields
         */
        validateKVMessage();
    }

	/**
	 * Copy constructor
	 * 
	 * @param kvm
	 */
	public KVMessage(KVMessage kvm) {
		this.msgType = kvm.msgType;
		this.key = kvm.key;
		this.value = kvm.value;
		this.message = kvm.message;
		this.tpcOpId = kvm.tpcOpId;
	}
    
    private void validateKVMessage() throws KVException {
        /*
         * Validate message format based on type
         */
        if (msgType.equals(GETREQ)) {
            if (!checkFormat(true, true, false, false, false))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
            validateKey();
        } else if (msgType.equals(PUTREQ)) {
            if (!checkFormat(true, true, true, false, true) &&
                !checkFormat(true, true, true, false, false))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
            validateKey();
            validateValue();
        } else if (msgType.equals(DELREQ)) {
            if (!checkFormat(true, true, false, false, true) &&
                !checkFormat(true, true, false, false, false))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
            validateKey();
        } else if (msgType.equals(RESP)) {
            if (!checkFormat(true, true, true, false, false) &&
                !checkFormat(true, false, false, true, false))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
            if (checkFormat(true, true, true, false, false)) {
                validateKey();
                validateValue();
            }
        } else if (msgType.equals(PUTTEST)) {
            if (!checkFormat(true, true, true, false, true) &&
                !checkFormat(true, true, true, false, false))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
            validateKey();
            validateValue();
        } else if (msgType.equals(DELTEST)) {
            if (!checkFormat(true, true, false, false, true) &&
                !checkFormat(true, true, false, false, false))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
            validateKey();
        } else if (msgType.equals(READY)) {
            if (!checkFormat(true, false, false, false, true))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
        } else if (msgType.equals(ABORT)) {
            if (!checkFormat(true, false, false, true, true) &&
                !checkFormat(true, false, false, false, true))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
        } else if (msgType.equals(COMMIT)) {
            if (!checkFormat(true, false, false, false, true))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
        } else if (msgType.equals(ACK)) {
            if (!checkFormat(true, false, false, false, true))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
        } else if (msgType.equals(REGISTER)) {
            if (!checkFormat(true, false, false, true, false))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);

            if (!isValidateRegisterMessage(message))
                throw new KVException(RESP, KVError.REGISTRATION_ERROR);
        } else if (msgType.equals(IGNORENEXT)) {
            if (!checkFormat(true, false, false, false, false))
                throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
	} else if (msgType.equals("reset")) {
	    return;
        } else {
            // invalid message type
            throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
        }
    }

    public static Matcher slaveInfoMatcher(String msg) {
        Pattern r = Pattern.compile(SLAVE_INFO_REGEX);
        Matcher m = r.matcher(msg);
        return m;
    }

    public static boolean isValidateRegisterMessage(String msg) {
        return slaveInfoMatcher(msg).find();
    }

    private void validateMessageType() throws KVException {
        String[] typeList = {GETREQ, PUTREQ, PUTTEST, DELREQ, DELTEST, RESP, READY, ABORT, COMMIT,
			     ACK, REGISTER, IGNORENEXT, "reset"};

        for(int i = 0; i < typeList.length; i++)
            if(typeList[i].equals(this.msgType))
                return;
    
        throw new KVException(RESP, KVError.MSG_FORMAT_INCORRECT);
    }

    private void validateValue() throws KVException {
        if (value == null)
            throw new KVException(KVError.INVALID_VALUE);
        if (value.length() <= 0)
            throw new KVException(KVError.INVALID_VALUE);
        if (value.length() > MAX_VALUE_SIZE)
            throw new KVException(KVError.OVERSIZED_VALUE);
    }
    
    private void validateKey() throws KVException {
        if (key == null)
            throw new KVException(KVError.INVALID_KEY);
        if (key.length() <= 0)
            throw new KVException(KVError.INVALID_KEY);
        if (key.length() > MAX_KEY_SIZE)
            throw new KVException(KVError.OVERSIZED_KEY);
    }
    
    
    /**
     * @return true if KVMessage has the following fields set to null or not null
     */
    private boolean checkFormat(boolean msgType, boolean key, boolean value, 
            boolean message, boolean tpcOpId) {
        return (this.msgType != null) == msgType &&
            (this.key != null) == key &&
            (this.value != null) == value &&
            (this.message != null) == message &&
            (this.tpcOpId != null) == tpcOpId;
    }
    
    /**
     * @return the text value of a tag
     */
    private String getElementValue(Document doc, String tagName) throws IOException {
        NodeList list = doc.getElementsByTagName(tagName);
        Node item = list.item(0);
        
        try {
            return item.getTextContent();
        } catch (NullPointerException e) {
            throw new IOException("Element not found");
        }
    }
    
    /**
     * @return the type of the document
     */
    private String getDocType(Document doc) throws IOException {
        NodeList list = doc.getElementsByTagName("KVMessage");
        Node item = list.item(0);
        
        try {
            return item.getAttributes().getNamedItem("type").getNodeValue();
        } catch (NullPointerException e) {
            throw new IOException("Element not found");
        }
    }
	
	
	/**
	 * Generate the XML representation for this message.
	 * @return the XML String
	 * @throws KVException if not enough data is available to generate a valid KV XML message
	 */
	public String toXML() throws KVException {
        /*
         * validate KVMessage
         */
        validateKVMessage();
        
        /*
         * create xml
         */
        String s = "";
        
        s += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        if(msgType.equals(IGNORENEXT) || msgType.equals("reset"))
            s += "<KVMessage type=\"" + getMsgType() + "\"/>";
        else
            s += "<KVMessage type=\"" + getMsgType() + "\">";
        
        if (getKey() != null)
            s += "<Key>"  + getKey() + "</Key>";
        
        if (getValue() != null)
            s += "<Value>" + getValue() + "</Value>";
        
        if (getMessage() != null)
            s += "<Message>" + getMessage() + "</Message>";

        if (getTpcOpId() != null)
            s += "<TPCOpId>" + getTpcOpId() + "</TPCOpId>";
        
        if(!msgType.equals(IGNORENEXT) && !msgType.equals("reset"))
            s += "</KVMessage>";
        return s;
	}
	
	public void sendMessage(Socket sock) throws KVException {
        try {
            OutputStream out = sock.getOutputStream();
            out.write(toXML().getBytes());
            out.flush();
            sock.shutdownOutput();
        } catch (IOException e) {
            throw new KVException(KVError.NETWORK_ERROR_SEND_DATA);
        }
	}

    public String debugString() {
        return "--- KVMessage ---" + "\n" + 
               "    msgType : " + msgType + "\n" + 
               "    key: " + key + "\n" + 
               "    value: " + value + "\n" + 
               "    message: " + message + "\n" + 
               "    tpcOpId: " + tpcOpId + "\n";
    }

    public void debugPrint() {
        System.err.println(debugString());
    }
}
