/**
 * Slave Server component of a KeyValue store
 *
 * @author Mosharaf Chowdhury (http://www.mosharaf.com)
 * @author Prashanth Mohan (http://www.cs.berkeley.edu/~prmohan)
 *
 * Copyright (c) 2012, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

/**
 * This class defines the slave key value servers. Each individual KVServer
 * would be a fully functioning Key-Value server. For Project 3, you would
 * implement this class. For Project 4, you will have a Master Key-Value server
 * and multiple of these slave Key-Value servers, each of them catering to a
 * different part of the key namespace.
 *
 */
public class KVServer implements KeyValueInterface {
    private KVStore dataStore = null;
    private KVCache dataCache = null;
    
    private static final int MAX_KEY_SIZE = 256;
    private static final int MAX_VAL_SIZE = 256 * 1024;
    
    /**
     * @param numSets number of sets in the data Cache.
     */
    public KVServer(int numSets, int maxElemsPerSet) {
        dataStore = new KVStore();
        dataCache = new KVCache(numSets, maxElemsPerSet);
        
        AutoGrader.registerKVServer(dataStore, dataCache);
    }
    
    public void put(String key, String value) throws KVException {
        // Must be called before anything else
        AutoGrader.agKVServerPutStarted(key, value);
        
        // Check key
        if (key == null || key.length() == 0) {
            throw new KVException(KVMessage.RESP, KVError.INVALID_KEY);
        }
        
        if (key.length() > MAX_KEY_SIZE) {
            throw new KVException(KVMessage.RESP, KVError.OVERSIZED_KEY);
        }
        
        // Check value
        if (value == null || value.length() == 0) {
            throw new KVException(KVMessage.RESP, KVError.INVALID_VALUE);
        }
        
        if (value.length() > MAX_VAL_SIZE) {
            throw new KVException(KVMessage.RESP, KVError.OVERSIZED_VALUE);
        }
        
        // Acquire lock
        WriteLock lock = dataCache.getWriteLock(key);
        lock.lock();
        
        // Write into dataStore and dataCache
        try {
            dataCache.put(key, value);
            dataStore.put(key, value);
        } catch (KVException e) {
            // Release lock
            lock.unlock();
            
            // Must be called before return or abnormal exit
            AutoGrader.agKVServerPutFinished(key, value);
            
            // Throw the original KVException
            throw e;
        }
        
        // Release lock
        lock.unlock();
        
        // Must be called before return or abnormal exit
        AutoGrader.agKVServerPutFinished(key, value);
    }
    
    public String get (String key) throws KVException {
        // Must be called before anything else
        AutoGrader.agKVServerGetStarted(key);
        
        // Check key
        if (key == null || key.length() == 0) {
            throw new KVException(KVMessage.RESP, KVError.INVALID_KEY);
        }
        
        if (key.length() > MAX_KEY_SIZE) {
            throw new KVException(KVMessage.RESP, KVError.OVERSIZED_KEY);
        }
        
        // Acquire lock
        WriteLock lock = dataCache.getWriteLock(key);
        lock.lock();
        
        // Try to get data from dataCache
        String value = dataCache.get(key);
        
        // If data not in KVCache, try to get it from dataStore and put it into dataCache
        if (value == null) {
            try {
                value = dataStore.get(key);
                dataCache.put(key, value);
            } catch (KVException e) {
                // Release lock
                lock.unlock();
                
                // Must be called before return or abnormal exit
                AutoGrader.agKVServerGetFinished(key);
                
                // Throw the original KVException
                throw e;
            }
        }
        
        // Release lock
        lock.unlock();
        
        // Must be called before return or abnormal exit
        AutoGrader.agKVServerGetFinished(key);
        
        return value;
    }
    
    public void del (String key) throws KVException {
        // Must be called before anything else
        AutoGrader.agKVServerDelStarted(key);
        
        // Check key
        if (key == null || key.length() == 0) {
            throw new KVException(KVMessage.RESP, KVError.INVALID_KEY);
        }
        
        if (key.length() > MAX_KEY_SIZE) {
            throw new KVException(KVMessage.RESP, KVError.OVERSIZED_KEY);
        }
        
        // Acquire lock
        WriteLock lock = dataCache.getWriteLock(key);
        lock.lock();
        
        // Delete data from dataCache
        dataCache.del(key);
        
        // Delete data from dataStore
        try {
            dataStore.get(key);
            dataStore.del(key);
        } catch (KVException e) {
            // Release lock
            lock.unlock();
            
            // Must be called before return or abnormal exit
            AutoGrader.agKVServerDelFinished(key);
            
            // Throw the original KVException
            throw e;
        }
        
        // Release lock
        lock.unlock();
        
        // Must be called before return or abnormal exit
        AutoGrader.agKVServerDelFinished(key);
    }
    
    public String makeDecision(KVMessage msg) {
        
        String key = msg.getKey();
        String value = msg.getValue();
        
        if (msg.getMsgType().equals(KVMessage.PUTREQ)) {
            // Check key
            if (key == null || key.length() == 0) {
                return KVError.INVALID_KEY.toString();
            }
            
            if (key.length() > MAX_KEY_SIZE) {
                return KVError.OVERSIZED_KEY.toString();
            }
            
            // Check value
            if (value == null || value.length() == 0) {
                return KVError.INVALID_VALUE.toString();
            }
            
            if (value.length() > MAX_VAL_SIZE) {
                return KVError.OVERSIZED_VALUE.toString();
            }
        } else if (msg.getMsgType().equals(KVMessage.DELREQ)) {
            // Check key
            if (key == null || key.length() == 0) {
                return KVError.INVALID_KEY.toString();
            }
            
            if (key.length() > MAX_KEY_SIZE) {
                return KVError.OVERSIZED_KEY.toString();
            }
            
            try {
                dataStore.get(key);
            } catch (KVException e) {
                return e.getMsg().getMessage();
            }
        }
        
        return null;
    }
    
    public void perform(KVMessage msg) throws KVException {
        
        if (msg == null) {
            return;
        }
        
        String key = msg.getKey();
        String value = msg.getValue();
        
        if (msg.getMsgType().equals(KVMessage.PUTREQ)) {
            put(key, value);
        }
        else if (msg.getMsgType().equals(KVMessage.DELREQ)) {
            del(key);
        }
        
    }
    
    // Getter methods for testing
    public KVStore getStore() {
        return dataStore;
    }
    
    public KVCache getCache() {
        return dataCache;
    }
    
    
    public boolean hasKey (String key) throws KVException {
	return false;
    }

    // for test
    public void reset() {
        dataStore = new KVStore();
        dataCache = new KVCache(100, 10);
    }
}
