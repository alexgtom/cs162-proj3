/**
 * This is a class for saving data in KVCache
 */

package edu.berkeley.cs162;

import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.HashMap;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class CacheSet {
    
    /**
     * block(page) in the set.
     */
    protected class Block {
    	protected boolean used;
    	protected String key;
    	protected String value;
    	protected boolean valid;
        
    	protected Block() {
            used = false;
            valid = false;
            key = null;
            value = null;
    	}
        
    }
    
    /**
     * search the block that contains the key
     */
    protected HashMap<String, Integer> search;
    
    /**
     * A LinkedList for performing second chance algorithm.
     */
    protected Block[] blocks;
    
    /**
     * A lock for preventing multiple threads from accessing this set.
     */
    protected WriteLock lock;
    
    /**
     * A Linked List of Available Blocks
     */
    protected LinkedList<Integer> available;
    
    /**
     * A Linked List for second chance algorithm to replace a page.
     */
    protected LinkedList<Integer> replace;
    
    /**
     * get value for corresponding key.
     */
    protected String get(String key) {
    	Integer blockId = search.get(key); // get id of the block the key is in
    	// if key doesn't exist in cache, return null
    	if (blockId == null) {
            return null;
    	}
    	// else find the block, set used bit, and return the value
    	else {
            Block block = blocks[blockId.intValue()];
            block.used = true;
            return block.value;
    	}
    }
    
    /**
     * put key-value pair into database.
     */
    protected void put(String key, String value) {
    	Integer blockId = search.get(key); // get id of the block the key is in
    	Block block;
    	// if key doesn't exist, we need to find a block to insert kv-pair
    	if (blockId == null) {
            //if no invalid block exists, run 2nd chance to make an invalid one
            if (available.size() == 0) {
                eviction();
            }
            // get an invalid block to put kv-pair
            blockId = available.poll();
            block = blocks[blockId.intValue()];
            // set valid bit, key, value
            block.valid = true;
            block.key = key;
            block.value = value;
            block.used = false;
            // associate the blockId with key
            search.put(key, blockId);
            // make it ready for replace
            replace.add(blockId);
    	}
    	// otherwise find the block and overwrite the value
    	else {
            block = blocks[blockId.intValue()];
            block.value = value;
            block.used = false;
            replace.remove(blockId);
            replace.add(blockId);
    	}
    }
    
    /**
     * delete key-value pair for corresponding key.
     */
    protected void del(String key) {
    	Integer blockId = search.get(key); // get id of the block the key is in
    	// if the block is found, make the block invalid
    	if (blockId != null) {
            Block block = blocks[blockId.intValue()];
            block.valid = false;
            search.remove(key);
            replace.remove(blockId);
            available.add(blockId);
    	}
    }
    
    protected CacheSet(int maxElements) {
    	search = new HashMap<String, Integer>();
    	blocks = new Block[maxElements];
    	available = new LinkedList<Integer>();
    	replace = new LinkedList<Integer>();
    	for (int i = 0; i < maxElements; i++) {
            blocks[i] = new Block();
            available.add(i);
    	}
    	lock = (new ReentrantReadWriteLock()).writeLock();
    }
    
    /**
     * remove a kv-pair by running second chance algorithm.
     */
    protected void eviction() {
    	Integer headId = replace.poll();
    	while(blocks[headId.intValue()].used) {
            blocks[headId.intValue()].used = false;
            replace.add(headId);
            headId = replace.poll();
    	}
    	blocks[headId.intValue()].valid = false;
    	search.remove(blocks[headId.intValue()].key);
    	available.add(headId);
    }
    
    // test constructor
    public static void test1() {
    	CacheSet set = new CacheSet(10);
    	assertTrue("not enough blocks are created", set.blocks.length == 10);
    	assertTrue("block is not created properly", set.blocks[0] instanceof Block);
    	assertTrue("lock is not created properly", set.lock instanceof WriteLock);
    	assertTrue("replace queue should be empty", set.replace.size() == 0);
    	assertTrue("not all blocks are available", set.available.size() == 10);
    	assertTrue("some blocks are used", set.search.size() == 0);
    	for (int i = 0; i < set.available.size(); i++) {
            assertTrue("available pages wrong", set.available.poll().intValue() == i);
            assertTrue("block key is corrupted", set.blocks[i].key == null);
            assertTrue("block value is corrupted", set.blocks[i].value == null);
            assertTrue("block used bit is corrupted", set.blocks[i].used == false);
            assertTrue("block valid bit is corrupted", set.blocks[i].valid == false);
    	}
    }
    
    // test put with no replacement, no overwrite
    public static void test2() {
    	CacheSet set = new CacheSet(2);
    	set.put("key1", "value1");
    	assertTrue("replace queue should have one block", set.replace.size() == 1);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
    	assertTrue("available pages size wrong", set.available.size() == 1);
    	assertTrue("available pages elements wrong", set.available.getFirst().intValue() == 1);
    	assertTrue("mapping size wrong", set.search.size() == 1);
    	assertTrue("mapping isn't correct", set.search.get("key1").intValue() == 0);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key == null);
        assertTrue("block1 value is corrupted", set.blocks[1].value == null);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == false);
    	set.put("key2", "value2");
    	assertTrue("replace queue should have two block", set.replace.size() == 2);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
    	assertTrue("replace pages elements wrong", set.replace.getLast().intValue() == 1);
    	assertTrue("available pages size wrong", set.available.size() == 0);
    	assertTrue("mapping size wrong", set.search.size() == 2);
    	assertTrue("mapping0 isn't correct", set.search.get("key1").intValue() == 0);
    	assertTrue("mapping1 isn't correct", set.search.get("key2").intValue() == 1);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key.equals("key2"));
        assertTrue("block1 value is corrupted", set.blocks[1].value.equals("value2"));
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == true);
    }
    
    // test put overwrite
    public static void test3() {
    	CacheSet set = new CacheSet(2);
    	set.put("key1", "value1");
    	set.put("key1", "value2");
    	assertTrue("replace queue should have one block", set.replace.size() == 1);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
    	assertTrue("available pages size wrong", set.available.size() == 1);
    	assertTrue("available pages elements wrong", set.available.getFirst().intValue() == 1);
    	assertTrue("mapping size wrong", set.search.size() == 1);
    	assertTrue("mapping isn't correct", set.search.get("key1").intValue() == 0);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value2"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key == null);
        assertTrue("block1 value is corrupted", set.blocks[1].value == null);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == false);
    }
    
    // test get with existing key
    public static void test4() {
    	CacheSet set = new CacheSet(2);
       	set.put("key1", "value1");
       	for (int i = 0; i < 1; i++) {
            assertTrue("get incorrect value", set.get("key1").equals("value1"));
            assertTrue("replace queue should have one block", set.replace.size() == 1);
            assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
            assertTrue("available pages size wrong", set.available.size() == 1);
            assertTrue("available pages elements wrong", set.available.getFirst().intValue() == 1);
            assertTrue("mapping size wrong", set.search.size() == 1);
            assertTrue("mapping isn't correct", set.search.get("key1").intValue() == 0);
            assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
            assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
            assertTrue("block0 used bit is corrupted", set.blocks[0].used == true);
            assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
            assertTrue("block1 key is corrupted", set.blocks[1].key == null);
            assertTrue("block1 value is corrupted", set.blocks[1].value == null);
            assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
            assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == false);
       	}
       	set.put("key2", "value2");
    	assertTrue("replace queue should have two block", set.replace.size() == 2);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
    	assertTrue("replace pages elements wrong", set.replace.getLast().intValue() == 1);
    	assertTrue("available pages size wrong", set.available.size() == 0);
    	assertTrue("mapping size wrong", set.search.size() == 2);
    	assertTrue("mapping0 isn't correct", set.search.get("key1").intValue() == 0);
    	assertTrue("mapping1 isn't correct", set.search.get("key2").intValue() == 1);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == true);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key.equals("key2"));
        assertTrue("block1 value is corrupted", set.blocks[1].value.equals("value2"));
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == true);
        assertTrue("get incorrect value", set.get("key2").equals("value2"));
    	assertTrue("replace queue should have two block", set.replace.size() == 2);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
    	assertTrue("replace pages elements wrong", set.replace.getLast().intValue() == 1);
    	assertTrue("available pages size wrong", set.available.size() == 0);
    	assertTrue("mapping size wrong", set.search.size() == 2);
    	assertTrue("mapping0 isn't correct", set.search.get("key1").intValue() == 0);
    	assertTrue("mapping1 isn't correct", set.search.get("key2").intValue() == 1);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == true);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key.equals("key2"));
        assertTrue("block1 value is corrupted", set.blocks[1].value.equals("value2"));
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == true);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == true);
    }
    
    // test get with not existing key
    public static void test5() {
    	CacheSet set = new CacheSet(2);
    	set.put("key1", "value1");
        assertTrue("get incorrect value", set.get("keyX") == null);
        assertTrue("replace queue should have one block", set.replace.size() == 1);
        assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
        assertTrue("available pages size wrong", set.available.size() == 1);
        assertTrue("available pages elements wrong", set.available.getFirst().intValue() == 1);
        assertTrue("mapping size wrong", set.search.size() == 1);
        assertTrue("mapping isn't correct", set.search.get("key1").intValue() == 0);
        assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
        assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
        assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
        assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key == null);
        assertTrue("block1 value is corrupted", set.blocks[1].value == null);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == false);
    	set.put("key2", "value2");
        assertTrue("get incorrect value", set.get("keyX") == null);
        assertTrue("replace queue should have two block", set.replace.size() == 2);
        assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
        assertTrue("replace pages elements wrong", set.replace.getLast().intValue() == 1);
    	assertTrue("available pages size wrong", set.available.size() == 0);
    	assertTrue("mapping size wrong", set.search.size() == 2);
    	assertTrue("mapping0 isn't correct", set.search.get("key1").intValue() == 0);
    	assertTrue("mapping1 isn't correct", set.search.get("key2").intValue() == 1);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key.equals("key2"));
        assertTrue("block1 value is corrupted", set.blocks[1].value.equals("value2"));
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == true);
    }
    
    // test eviction consistency
    public static void test6() {
    	CacheSet set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
    	set.eviction();
    	assertTrue("replace queue should have two block", set.replace.size() == 2);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 1);
    	assertTrue("replace pages elements wrong", set.replace.getLast().intValue() == 2);
    	assertTrue("available pages size wrong", set.available.size() == 1);
        assertTrue("available pages elements wrong", set.available.getFirst().intValue() == 0);
    	assertTrue("mapping size wrong", set.search.size() == 2);
    	assertTrue("mapping1 isn't correct", set.search.get("key2").intValue() == 1);
    	assertTrue("mapping2 isn't correct", set.search.get("key3").intValue() == 2);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == false);
        assertTrue("block1 key is corrupted", set.blocks[1].key.equals("key2"));
        assertTrue("block1 value is corrupted", set.blocks[1].value.equals("value2"));
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == true);
        assertTrue("block2 key is corrupted", set.blocks[2].key.equals("key3"));
        assertTrue("block2 value is corrupted", set.blocks[2].value.equals("value3"));
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == false);
        assertTrue("block2 valid bit is corrupted", set.blocks[2].valid == true);
    }
    
    // test eviction choice
    public static void test7() {
    	CacheSet set;
    	// case1: used = 0, 0, 0; expected: remove block 0
    	set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
    	set.blocks[0].used = false;
    	set.blocks[1].used = false;
    	set.blocks[2].used = false;
    	set.eviction();
        assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == false);
    	assertTrue("replaced wrong block", set.available.getFirst().intValue() == 0);
    	// case2: used = 0, 0, 1; expected: remove block 0
       	set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
    	set.blocks[0].used = false;
    	set.blocks[1].used = false;
    	set.blocks[2].used = true;
    	set.eviction();
        assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == true);
    	assertTrue("replaced wrong block", set.available.getFirst().intValue() == 0);
    	// case3: used = 0, 1, 0; expected: remove block 0
       	set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
    	set.blocks[0].used = false;
    	set.blocks[1].used = true;
    	set.blocks[2].used = false;
    	set.eviction();
        assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == true);
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == false);
    	assertTrue("replaced wrong block", set.available.getFirst().intValue() == 0);
    	// case4: used = 0, 1, 1; expected: remove block 0
       	set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
    	set.blocks[0].used = false;
    	set.blocks[1].used = true;
    	set.blocks[2].used = true;
    	set.eviction();
        assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == true);
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == true);
    	assertTrue("replaced wrong block", set.available.getFirst().intValue() == 0);
    	// case5: used = 1, 0, 0; expected: remove block 1
       	set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
    	set.blocks[0].used = true;
    	set.blocks[1].used = false;
    	set.blocks[2].used = false;
    	set.eviction();
        assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == false);
    	assertTrue("replaced wrong block", set.available.getFirst().intValue() == 1);
    	// case6: used = 1, 0, 1; expected: remove block 1
       	set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
    	set.blocks[0].used = true;
    	set.blocks[1].used = false;
    	set.blocks[2].used = true;
    	set.eviction();
        assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == true);
    	assertTrue("replaced wrong block", set.available.getFirst().intValue() == 1);
    	// case7: used = 1, 1, 0; expected: remove block 2
       	set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
    	set.blocks[0].used = true;
    	set.blocks[1].used = true;
    	set.blocks[2].used = false;
    	set.eviction();
        assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == false);
    	assertTrue("replaced wrong block", set.available.getFirst().intValue() == 2);
    	// case8: used = 1, 1, 1; expected: remove block 0
       	set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
    	set.blocks[0].used = true;
    	set.blocks[1].used = true;
    	set.blocks[2].used = true;
    	set.eviction();
        assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == false);
    	assertTrue("replaced wrong block", set.available.getFirst().intValue() == 0);
    }
    
    
    // test delete with existing key
    public static void test8() {
    	CacheSet set = new CacheSet(2);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.del("key1");
    	assertTrue("replace queue should have two block", set.replace.size() == 1);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 1);
    	assertTrue("available pages size wrong", set.available.size() == 1);
        assertTrue("available pages elements wrong", set.available.getFirst().intValue() == 0);
    	assertTrue("mapping size wrong", set.search.size() == 1);
    	assertTrue("mapping1 isn't correct", set.search.get("key2").intValue() == 1);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == false);
        assertTrue("block1 key is corrupted", set.blocks[1].key.equals("key2"));
        assertTrue("block1 value is corrupted", set.blocks[1].value.equals("value2"));
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == true);
    	
    }
    
    // test delete with non-existing key
    public static void test9() {
    	CacheSet set = new CacheSet(2);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.del("keyX");
    	assertTrue("replace queue should have two block", set.replace.size() == 2);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
    	assertTrue("replace pages elements wrong", set.replace.getLast().intValue() == 1);
    	assertTrue("available pages size wrong", set.available.size() == 0);
    	assertTrue("mapping size wrong", set.search.size() == 2);
    	assertTrue("mapping0 isn't correct", set.search.get("key1").intValue() == 0);
    	assertTrue("mapping1 isn't correct", set.search.get("key2").intValue() == 1);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key.equals("key2"));
        assertTrue("block1 value is corrupted", set.blocks[1].value.equals("value2"));
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == true);
    }
    
    // test put with replacement
    public static void test10() {
       	CacheSet set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
        set.get("key1");
        set.put("key4", "value4");
    	assertTrue("replace queue should have three blocks", set.replace.size() == 3);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 2);
    	assertTrue("replace pages elements wrong", set.replace.getLast().intValue() == 1);
    	assertTrue("available pages size wrong", set.available.size() == 0);
    	assertTrue("mapping size wrong", set.search.size() == 3);
    	assertTrue("mapping0 isn't correct", set.search.get("key1").intValue() == 0);
    	assertTrue("mapping1 isn't correct", set.search.get("key4").intValue() == 1);
    	assertTrue("mapping2 isn't correct", set.search.get("key3").intValue() == 2);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key.equals("key4"));
        assertTrue("block1 value is corrupted", set.blocks[1].value.equals("value4"));
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == true);
        assertTrue("block2 key is corrupted", set.blocks[2].key.equals("key3"));
        assertTrue("block2 value is corrupted", set.blocks[2].value.equals("value3"));
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == false);
        assertTrue("block2 valid bit is corrupted", set.blocks[2].valid == true);
    }
    
    // test overwrite
    public static void test11() {
       	CacheSet set = new CacheSet(3);
    	set.put("key1", "value1");
    	set.put("key2", "value2");
    	set.put("key3", "value3");
        set.get("key2");
        set.put("key2", "value4");
    	assertTrue("replace queue should have three blocks", set.replace.size() == 3);
    	assertTrue("replace pages elements wrong", set.replace.getFirst().intValue() == 0);
    	assertTrue("replace pages elements wrong", set.replace.getLast().intValue() == 1);
    	assertTrue("available pages size wrong", set.available.size() == 0);
    	assertTrue("mapping size wrong", set.search.size() == 3);
    	assertTrue("mapping0 isn't correct", set.search.get("key1").intValue() == 0);
    	assertTrue("mapping1 isn't correct", set.search.get("key2").intValue() == 1);
    	assertTrue("mapping2 isn't correct", set.search.get("key3").intValue() == 2);
    	assertTrue("block0 key is corrupted", set.blocks[0].key.equals("key1"));
    	assertTrue("block0 value is corrupted", set.blocks[0].value.equals("value1"));
    	assertTrue("block0 used bit is corrupted", set.blocks[0].used == false);
    	assertTrue("block0 valid bit is corrupted", set.blocks[0].valid == true);
        assertTrue("block1 key is corrupted", set.blocks[1].key.equals("key2"));
        assertTrue("block1 value is corrupted", set.blocks[1].value.equals("value4"));
        assertTrue("block1 used bit is corrupted", set.blocks[1].used == false);
        assertTrue("block1 valid bit is corrupted", set.blocks[1].valid == true);
        assertTrue("block2 key is corrupted", set.blocks[2].key.equals("key3"));
        assertTrue("block2 value is corrupted", set.blocks[2].value.equals("value3"));
        assertTrue("block2 used bit is corrupted", set.blocks[2].used == false);
        assertTrue("block2 valid bit is corrupted", set.blocks[2].valid == true);
    }	
	
    public String toXML() {
        String s = "";
        
        for (int i = 0; i < blocks.length; i++) {
            Block block = blocks[i];
            s += "<CacheEntry isReferenced=\"" + block.used + "\"" + 
            " isValid=\"" + block.valid + "\">";
            s += "  <Key>" + block.key + "</Key>";
            s += "  <Value>" + block.value + "</Value>";
            s += "</CacheEntry>";
        }
        return s;
    }
}
