/**
 * Handle TPC connections over a socket interface
 *
 * @author Mosharaf Chowdhury (http://www.mosharaf.com)
 * @author Prashanth Mohan (http://www.cs.berkeley.edu/~prmohan)
 *
 * Copyright (c) 2012, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.ConnectException;

/**
 * Implements NetworkHandler to handle 2PC operation requests from the Master/
 * Coordinator Server
 *
 */
public class TPCMasterHandler implements NetworkHandler {
    private KVServer kvServer = null;
    private ThreadPool threadpool = null;
    private TPCLog tpcLog = null;
    
    private long slaveID = -1;
    
    // Used to handle the "ignoreNext" message
    private boolean ignoreNext = false;
    
    // States carried from the first to the second phase of a 2PC operation
    // Reset state should correspond to the original value
    private KVMessage originalMessage = null;
    private boolean aborted = true;
    
    
    public TPCMasterHandler(KVServer keyserver) {
        this(keyserver, 1);
    }
    
    public TPCMasterHandler(KVServer keyserver, long slaveID) {
        this.kvServer = keyserver;
        this.slaveID = slaveID;
        threadpool = new ThreadPool(1);
    }
    
    public TPCMasterHandler(KVServer kvServer, long slaveID, int connections) {
        this.kvServer = kvServer;
        this.slaveID = slaveID;
        threadpool = new ThreadPool(connections);
    }
    
    protected String ignoreNextMsg() {
        return "IgnoreNextError: SlaveServer " + slaveID + " has ignored this 2PC request during the first phase";
    }
    
    private class ClientHandler implements Runnable {
        private KVServer keyserver = null;
        private Socket client = null;
        
        private void closeConn() {
            try {
                client.close();
            } catch (IOException e) {
            }
        }
        
        @Override
        public void run() {
            // Receive message from client
            KVMessage msg = null;
            try {
                // Do not add timeout. There might be significant delay between
                // relaying the first message from the client to the client handler
                msg = new KVMessage(client);
            } catch (KVException e) {
                System.out.println(e.getMessage());
                closeConn();
                return;
            }
            
            // Parse the message and do stuff
            String key = msg.getKey();
            
            if (msg.getMsgType().equals(KVMessage.PUTREQ)) {
                handlePut(msg, key);
            } else if (msg.getMsgType().equals(KVMessage.PUTTEST)) {
                handlePutTest(msg, key);
            } else if (msg.getMsgType().equals(KVMessage.GETREQ)) {
                handleGet(msg, key);
            } else if (msg.getMsgType().equals(KVMessage.DELREQ)) {
                handleDel(msg, key);
            } else if (msg.getMsgType().equals(KVMessage.DELTEST)) {
                handleDelTest(msg, key);
            } else if (msg.getMsgType().equals(KVMessage.IGNORENEXT)) {
                // Set ignoreNext to true. PUT and DEL handlers know what to do.
                ignoreNext = true;
                
                // Send back an acknowledgment
                try {
                    KVMessage ack = new KVMessage(KVMessage.RESP);
                    ack.setMessage(KVError.SUCCESS.toString());
                    ack.sendMessage(client);
                } catch (Exception e) {
                    // Ignore any exception
                }
            } else if (msg.getMsgType().equals(KVMessage.COMMIT) || msg.getMsgType().equals(KVMessage.ABORT)) {
                // Check in TPCLog for the case when SlaveServer is restarted
                if(tpcLog.hasInterruptedTpcOperation()) {
                    originalMessage = tpcLog.getInterruptedTpcOperation();
                    
                    if (originalMessage != null) {
                        if (kvServer.makeDecision(originalMessage) == null) {
                            aborted = false;
                        } else {
                            aborted = true;
                        }
                    }
                }
                
                handleMasterResponse(msg, originalMessage, aborted);
                
                // Reset state
                originalMessage = null;
                aborted = true;
            }
            else if (msg.getMsgType().equals("reset")) {
		reset();
		kvServer.reset();
		try {
		    tpcLog.rebuildKeyServer();
		}
		catch (Exception e) {
		}
	    }
	    
            // Finally, close the connection
            closeConn();
        }
        
        private void handlePut(KVMessage msg, String key) {
            AutoGrader.agTPCPutStarted(slaveID, msg, key);
            
            // Store for use in the second phase
            originalMessage = new KVMessage(msg);
            
            KVMessage resp;
            if (ignoreNext) {
                ignoreNext = false;
                try {
                    resp = new KVMessage(KVMessage.ABORT, ignoreNextMsg());
                    resp.setTpcOpId(msg.getTpcOpId());
                    resp.sendMessage(client);
                    aborted = true;
                } catch (Exception e) {
                    //Ignore any exception
                }
            } else {
                tpcLog.appendAndFlush(new KVMessage(msg));
                String err = kvServer.makeDecision(msg);
                if (err == null) {
                    try {
                        resp = new KVMessage(KVMessage.READY);
                        resp.setTpcOpId(msg.getTpcOpId());
                        resp.sendMessage(client);
                        aborted = false;
                    } catch (Exception e) {
                        // Ignore any exception
                    }
                } else {
                    try {
                        resp = new KVMessage(KVMessage.ABORT, err);
                        resp.setTpcOpId(msg.getTpcOpId());
                        resp.sendMessage(client);
                        aborted = true;
                    } catch (Exception e) {
                        // Ignore any exception
                    }
                }
            }
            
            AutoGrader.agTPCPutFinished(slaveID, msg, key);
        }

        private void handlePutTest(KVMessage msg, String key) {
            AutoGrader.agTPCPutStarted(slaveID, msg, key);
            
            // Store for use in the second phase
            originalMessage = new KVMessage(msg);
            
            KVMessage resp;
            if (ignoreNext) {
                ignoreNext = false;
                try {
                    resp = new KVMessage(KVMessage.ABORT, ignoreNextMsg());
                    //resp.setTpcOpId(msg.getTpcOpId());
                    //resp.sendMessage(client);
                    aborted = true;
                } catch (Exception e) {
                    //Ignore any exception
                }
            } else {
                tpcLog.appendAndFlush(new KVMessage(msg));
                String err = kvServer.makeDecision(msg);
            }
        }

        private void handleGet(KVMessage msg, String key) {
            AutoGrader.agGetStarted(slaveID);
            
            KVMessage resp;
            String value;
            try {
                value = kvServer.get(key);
            } catch (KVException e) {
                try {
                    resp = e.getMsg();
                    resp.sendMessage(client);
                    AutoGrader.agGetFinished(slaveID);
                    return;
                } catch (KVException e2) {
                    AutoGrader.agGetFinished(slaveID);
                    return;
                }
            }
            
            try {
                resp = new KVMessage(KVMessage.RESP);
                resp.setKey(key);
                resp.setValue(value);
                resp.sendMessage(client);
            } catch (KVException e) {
                AutoGrader.agGetFinished(slaveID);
                return;
            }
            
            
            AutoGrader.agGetFinished(slaveID);
        }
        
        private void handleDel(KVMessage msg, String key) {
            AutoGrader.agTPCDelStarted(slaveID, msg, key);
            
            // Store for use in the second phase
            originalMessage = new KVMessage(msg);
            
            KVMessage resp;
            if (ignoreNext) {
                ignoreNext = false;
                try {
                    resp = new KVMessage(KVMessage.ABORT, ignoreNextMsg());
                    resp.setTpcOpId(msg.getTpcOpId());
                    resp.sendMessage(client);
                    aborted = true;
                } catch (KVException e) {
                    //Ignore any exception
                }
            } else {
                tpcLog.appendAndFlush(new KVMessage(msg));
                String err = kvServer.makeDecision(msg);
                if (err == null) {
                    try {
                        resp = new KVMessage(KVMessage.READY);
                        resp.setTpcOpId(msg.getTpcOpId());
                        resp.sendMessage(client);
                        aborted = false;
                    } catch (KVException e) {
                        // Ignore any exception
                    }
                } else {
                    try {
                        resp = new KVMessage(KVMessage.ABORT, err);
                        resp.setTpcOpId(msg.getTpcOpId());
                        resp.sendMessage(client);
                        aborted = true;
                    } catch (KVException e) {
                        // Ignore any exception
                    }
                }
            }
            
            AutoGrader.agTPCDelFinished(slaveID, msg, key);
        }

        private void handleDelTest(KVMessage msg, String key) {
            AutoGrader.agTPCDelStarted(slaveID, msg, key);
            
            // Store for use in the second phase
            originalMessage = new KVMessage(msg);
            
            KVMessage resp;
            tpcLog.appendAndFlush(new KVMessage(msg));
                String err = kvServer.makeDecision(msg);
                if (err == null) {
                    try {
                        resp = new KVMessage(KVMessage.READY);
                        resp.setTpcOpId(msg.getTpcOpId());
                        aborted = false;
                    } catch (KVException e) {
                        // Ignore any exception
                    }
                }
            AutoGrader.agTPCDelFinished(slaveID, msg, key);
        }


        
        /**
         * Second phase of 2PC
         *
         * @param masterResp Global decision taken by the master
         * @param origMsg Message from the actual client (received via the coordinator/master)
         * @param origAborted Did this slave server abort it in the first phase
         */
        private void handleMasterResponse(KVMessage masterResp, KVMessage origMsg, boolean origAborted) {
            AutoGrader.agSecondPhaseStarted(slaveID, origMsg, origAborted);
            
            // Exclude the case ignored phase 1 and crashed after that, because we did not write to log in that case.
            KVMessage logMsg = tpcLog.getEntries().get(tpcLog.getEntries().size()-1);
            boolean done = (masterResp.getMsgType()).equals(logMsg.getMsgType()) && (masterResp.getTpcOpId()).equals(logMsg.getTpcOpId());
            
            if (!done) {
                if (!origAborted && masterResp.getMsgType().equals(KVMessage.COMMIT)) {
                    try {
                        kvServer.perform(origMsg);
                    } catch (KVException e) {
                        try {
                            KVMessage err = e.getMsg();
                            err.setTpcOpId(masterResp.getTpcOpId());
                            err.sendMessage(client);
                        } catch (KVException e2) {
                            // Ignore any exception
                        }
                    }
                }
                
                if (origMsg != null) {
                    tpcLog.appendAndFlush(masterResp);
                }
            }
            
            try {
                KVMessage ack = new KVMessage(KVMessage.ACK);
                ack.setTpcOpId(masterResp.getTpcOpId());
                ack.sendMessage(client);
            } catch (KVException e) {
                // Ignore any exception
            }
            
            AutoGrader.agSecondPhaseFinished(slaveID, origMsg, origAborted);
        }
        
        public ClientHandler(KVServer keyserver, Socket client) {
            this.keyserver = keyserver;
            this.client = client;
        }
    }
    
    @Override
    public void handle(Socket client) throws IOException {
        AutoGrader.agReceivedTPCRequest(slaveID);
        Runnable r = new ClientHandler(kvServer, client);
        try {
            threadpool.addToQueue(r);
        } catch (InterruptedException e) {
            // TODO: HANDLE ERROR
	    Thread.currentThread().interrupt();
            return;
        }        
        AutoGrader.agFinishedTPCRequest(slaveID);
    }
    
    /**
     * Set TPCLog after it has been rebuilt
     * @param tpcLog
     */
    public void setTPCLog(TPCLog tpcLog) {
        this.tpcLog = tpcLog;
    }

    // for test
    public void reset() {
	originalMessage = null;
	aborted = true;	
    }
    
    /**
     * Registers the slave server with the coordinator
     * 
     * @param masterHostName
     * @param servr KVServer used by this slave server (contains the hostName and a random port)
     * @throws UnknownHostException
     * @throws IOException
     * @throws KVException
     */
    public void registerWithMaster(String masterHostName, SocketServer server) throws UnknownHostException, IOException, KVException {
        AutoGrader.agRegistrationStarted(slaveID);
        
        Boolean run = true;
        while(run) {
            try {
                Socket master = new Socket(masterHostName, 9090);
                KVMessage regMessage = new KVMessage("register", slaveID + "@" + server.getHostname() + ":" + server.getPort());
                regMessage.sendMessage(master);
                
                // Receive master response. 
                // Response should always be success, except for Exceptions. Throw away.
                new KVMessage(master);
                master.close();
                run = false;
            } catch (Exception e) {
                // Ignore any exception
            }
        }
        
        AutoGrader.agRegistrationFinished(slaveID);
    }
}
