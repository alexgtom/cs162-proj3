package edu.berkeley.cs162;

public enum KVError {
    // This classes follows the error pattern described at:
    // http://stackoverflow.com/questions/446663/best-way-to-define-error-codes-strings-in-java
    // http://stackoverflow.com/questions/3978654/java-string-enum

    // Message format incorrect
    MSG_FORMAT_INCORRECT("Message format incorrect"),
    
    // There were no errors
    SUCCESS("Success"), 

    // If there is an error sending data
    NETWORK_ERROR_SEND_DATA("Network Error: Could not send data"), 

    // If there is an error receiving data
    NETWORK_ERROR_RECEIVE_DATA("Network Error: Could not receive data"), 

    // Could not connect to the server, port tuple
    NETWORK_ERROR_CONNECT("Network Error: Could not connect"), 

    // Error creating a socket
    NETWORK_ERROR_SOCKET("Network Error: Could not create socket"), 

    // Received a malformed message
    XML_ERROR("XML Error: Received unparseable message"), 

    // In the case that the submitted key is over 256 bytes (does not apply for 
    // get or del requests)
    OVERSIZED_KEY("Oversized key"), 

    // In the case that the submitted value is over 256 kilobytes
    OVERSIZED_VALUE("Oversized value"), 

    // If there was an error raised by KVStore
    IO_ERROR("IO Error"), 

    // For GET/DELETE requests if the corresponding key does not already exist 
    // in the store
    DOES_NOT_EXIST("Does not exist"), 

    // For any other error. Fill out "error-description" with your own 
    // description text.
    UNKNOWN_ERROR("Unknown Error: error-description"),

    INVALID_KEY("Unknown Error: Key is null or length is zero"),
    INVALID_VALUE("Unknown Error: Value is null or length is zero"),
    
    // Registration information was not in "slaveID@hostName:port" format."
    REGISTRATION_ERROR("Registration Error: Received unparseable slave information"),

    INVALID_OPERATION("Unknown Error: invalid operation"),

    TIMEOUT("Unknown Error: timeout");
    

    /**
     * @param text
     */
    private KVError(final String text) {
        this.text = text;
    }

    private final String text;

    @Override
    public String toString() {
        return text;
    }
}
