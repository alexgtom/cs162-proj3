/**
 * Persistent Key-Value storage layer. Current implementation is transient,
 * but assume to be backed on disk when you do your project.
 *
 * @author Mosharaf Chowdhury (http://www.mosharaf.com)
 * @author Prashanth Mohan (http://www.cs.berkeley.edu/~prmohan)
 *
 * Copyright (c) 2012, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Enumeration;
import java.io.FileWriter;
import java.io.IOException;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import edu.berkeley.cs162.*;
import java.io.IOException;
import org.xml.sax.SAXException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.SecurityException;

/**
 * This is a dummy KeyValue Store. Ideally this would go to disk,
 * or some other backing store. For this project, we simulate the disk like
 * system using a manual delay.
 *
 */
public class KVStore implements KeyValueInterface {
	private Dictionary<String, String> store 	= null;
	
	public KVStore() {
		resetStore();
	}
    
    public int size() {
        return store.size();
    }
    
	private void resetStore() {
		store = new Hashtable<String, String>();
	}
	
	public void put(String key, String value) throws KVException {
		AutoGrader.agStorePutStarted(key, value);
		
		try {
			putDelay();
			store.put(key, value);
		} finally {
			AutoGrader.agStorePutFinished(key, value);
		}
	}
	
	public String get(String key) throws KVException {
		AutoGrader.agStoreGetStarted(key);
		
		try {
			getDelay();
			String retVal = this.store.get(key);
			if (retVal == null) {
			    KVMessage msg = new KVMessage("resp", KVError.DOES_NOT_EXIST);
			    throw new KVException(msg);
			}
			return retVal;
		} finally {
			AutoGrader.agStoreGetFinished(key);
		}
	}
	
	public void del(String key) throws KVException {
		AutoGrader.agStoreDelStarted(key);

		try {
			delDelay();
			if(key != null)
				this.store.remove(key);
			else {
			    KVMessage msg = new KVMessage("resp", KVError.DOES_NOT_EXIST);
			    throw new KVException(msg);
			}
		} finally {
			AutoGrader.agStoreDelFinished(key);
		}
	}
	
	private void getDelay() {
		AutoGrader.agStoreDelay();
	}
	
	private void putDelay() {
		AutoGrader.agStoreDelay();
	}
	
	private void delDelay() {
		AutoGrader.agStoreDelay();
	}
	
    public String toXML() {
        String s = "";
        
        s += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        s += "<KVCache>";
        
        for (Enumeration<String> e = store.keys(); e.hasMoreElements();)
        {
            String key = e.nextElement();
            s += "<KVPair>";
            s += "<Key>";
            s += key;
            s += "</Key>";
            s += "<Value>";
            s += store.get(key);
            s += "</Value>";
            s += "</KVPair>";
        }
        
        s += "</KVCache>";
        return s;
    }
    
    public void dumpToFile(String fileName) throws KVException {
        try {
            FileWriter fp = new FileWriter(fileName);
            fp.write(toXML());
            fp.close();
        } catch (IOException e) {
        }
    }
    
    /**
     * Replaces the contents of the store with the contents of a file
     * written by dumpToFile; the previous contents of the store are lost.
     * @param fileName the file to be read.
     */
    public void restoreFromFile(String fileName) throws KVException {
        // reset
        resetStore();
        
        // set variables for xml parsing
        InputStream input = null;
        try {
            input = new FileInputStream(fileName);
        } catch (Exception e) {
            // ignore any exception
        }
        
        DocumentBuilderFactory builderFactory = null;
        DocumentBuilder builder = null;
        Document doc = null;
        
        try {
            builderFactory = DocumentBuilderFactory.newInstance();
            builder = builderFactory.newDocumentBuilder();
        } catch (Exception e) {
            // ignore any exception
        }
        
        try {
            doc = builder.parse(input);
        } catch (Exception e) {
            // ignore any exception
        }
        
        // parse
        NodeList nodeList = doc.getElementsByTagName("KVPair");
        
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NodeList childNodes = item.getChildNodes();
            
            Node key = childNodes.item(0);
            Node value = childNodes.item(1);
            
            try {
                put(key.getTextContent(), value.getTextContent());
            } catch (Exception e) {
                // ignore any exception
            }
        }
        
    }
}
