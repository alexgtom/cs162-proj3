/**
 * Master for Two-Phase Commits
 *
 * @author Mosharaf Chowdhury (http://www.mosharaf.com)
 * @author Prashanth Mohan (http://www.cs.berkeley.edu/~prmohan)
 *
 * Copyright (c) 2012, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.BrokenBarrierException;
import java.util.ArrayList;

public class TPCMaster {
    
    /**
     * Implements NetworkHandler to handle registration requests from
     * SlaveServers.
     *
     */
    private class TPCRegistrationHandler implements NetworkHandler {
        
        private ThreadPool threadpool = null;
        
        public TPCRegistrationHandler() {
            // Call the other constructor
            this(1);
        }
        
        public TPCRegistrationHandler(int connections) {
            threadpool = new ThreadPool(connections);
        }
        
        @Override
        public void handle(Socket client) throws IOException {
            Runnable r = new RegistrationHandler(client);
            try {
                threadpool.addToQueue(r);
            } catch (InterruptedException e) {
                // Ignore this error
                return;
            }
        }
        
        private class RegistrationHandler implements Runnable {
            
            
            private Socket client = null;
            
            public RegistrationHandler(Socket client) {
                this.client = client;
            }
            
            @Override
            public void run() {
                try {
                    // Get registration request
                    KVMessage request = new KVMessage(this.client, TIMEOUT_MILLISECONDS);
                    
                    // Throw exception if the request is not for registration
                    if (!request.getMsgType().equals(KVMessage.REGISTER)) {
                        throw new KVException(KVMessage.RESP, KVError.REGISTRATION_ERROR);
                    }
                    
                    // Register slave server
                    String slaveHost = request.getMessage();
                    SlaveInfo slave = new SlaveInfo(slaveHost);
                    long slaveId = slave.getSlaveID();
                    
                    if (!slaveIds.contains(slaveId)) {
                        slaveIds.add(slaveId);
                        slaveMap.put(slaveId, slave);
                    } else {
                        SlaveInfo oldInfo = slaveMap.get(slaveId);
                        oldInfo.setHostName(slave.getHostName());
                        oldInfo.setPort(slave.getPort());
                    }
                    
                    // send back ACK to client
                    KVMessage ack = new KVMessage(KVMessage.RESP, "Successfully registered " + slaveHost);
                    ack.sendMessage(this.client);
                } catch (KVException e) {
                    // Send error to client
                    try {
                        e.getMsg().sendMessage(this.client);
                        return;
                    } catch (Exception ex) {
                        // Ignore any exception
                    }
                } catch (Exception e) {
                    // Ignore any other exception
                }
            }
        }
    }
    
    /**
     *  Data structure to maintain information about SlaveServers
     *
     */
    private class SlaveInfo {
        // 64-bit globally unique ID of the SlaveServer
        private long slaveID = -1;
        // Name of the host this SlaveServer is running on
        private String hostName = null;
        // Port which SlaveServer is listening to
        private int port = -1;
        
        /**
         *
         * @param slaveInfo as "SlaveServerID@HostName:Port"
         * @throws KVException
         */
        public SlaveInfo(String slaveInfo) throws KVException {
            try {
                Matcher m = KVMessage.slaveInfoMatcher(slaveInfo);
                m.find();
                slaveID = hashTo64bit(m.group(1));
                hostName = m.group(2);
                port = Integer.parseInt(m.group(3));
            } catch (Exception e) {
                throw new KVException(KVMessage.RESP, KVError.REGISTRATION_ERROR);
            }
        }
        
        public long getSlaveID() {
            return slaveID;
        }
        
        public String getHostName() {
            return hostName;
        }
        
        public int getPort() {
            return port;
        }
        
        public void setHostName(String hn) {
            hostName = hn;
        }
        
        public void setPort(int p) {
            port = p;
        }
        
        public Socket connectHost() throws KVException {
            try {
                return new Socket(hostName, port);
            } catch (Exception e) {
                throw new KVException(KVMessage.RESP, e.getMessage());
            }
        }
        
        public void closeHost(Socket sock) throws KVException {
            try {
                sock.close();
            } catch (Exception e) {
                throw new KVException(KVMessage.RESP, e.getMessage());
            }
        }
        
        public String getErrorSegment(KVException err) {
            return "@" + getSlaveID() + ":=" + err.getMessage();
        }
    }
    
    // Timeout value used during 2PC operations
    public static final int TIMEOUT_MILLISECONDS = 5000;
    
    // Cache stored in the Master/Coordinator Server
    private KVCache masterCache = new KVCache(100, 10);
    
    // Registration server that uses TPCRegistrationHandler
    private SocketServer regServer = null;
    
    // Number of slave servers in the system
    private int numSlaves = -1;
    
    // ID of the next 2PC operation
    private Long tpcOpId = 0L;
    
    // Used for deciding which slaves to use
    private RangeMap<SlaveInfo> slaveMap = new RangeMap<SlaveInfo>();
    
    // Keep the slaveIDs unique
    private ArrayList<Long> slaveIds;
    
    // Slave Map Lock
    private static final ReentrantLock nextTpcOpIdLock = new ReentrantLock();

    /**
     * Creates TPCMaster
     *
     * @param numSlaves number of expected slave servers to register
     * @throws Exception
     */
    public TPCMaster() {

    }

    public TPCMaster(int numSlaves) {
        // Using SlaveInfos from command line just to get the expected number of SlaveServers
        this.numSlaves = numSlaves;
        
        // Keep the slaveIDs unique
        slaveIds = new ArrayList<Long>();
        
        // Create registration server
        regServer = new SocketServer("localhost", 9090);
        
        // Add handler
        regServer.addHandler(new TPCRegistrationHandler(numSlaves));
    }
    
    public int getNumSlaves() {
        return numSlaves;
    }
    
    /**
     * Calculates tpcOpId to be used for an operation. In this implementation
     * it is a long variable that increases by one for each 2PC operation.
     *
     * @return
     */
    private String getNextTpcOpId() {
        nextTpcOpIdLock.lock();
        tpcOpId++;
        nextTpcOpIdLock.unlock();
        return tpcOpId.toString();
    }
    
    /**
     * Start registration server in a separate thread
     */
    public void run() {
        AutoGrader.agTPCMasterStarted();
        Thread t = new Thread() {
            public void run() {
                try {
                    regServer.connect();
                    regServer.run();
                } catch (Exception e) {
                    // Ignore any exceptions
                }
            }
        };
        
        t.start();
        AutoGrader.agTPCMasterFinished();
    }
    
    /**
     * Converts Strings to 64-bit longs
     * Borrowed from http://stackoverflow.com/questions/1660501/what-is-a-good-64bit-hash-function-in-java-for-textual-strings
     * Adapted from String.hashCode()
     * @param string String to hash to 64-bit
     * @return
     */
    private long hashTo64bit(String string) {
        // Take a large prime
        long h = 1125899906842597L;
        int len = string.length();
        
        for (int i = 0; i < len; i++) {
            h = 31*h + string.charAt(i);
        }
        return h;
    }
    
    /**
     * Compares two longs as if they were unsigned (Java doesn't have unsigned data types except for char)
     * Borrowed from http://www.javamex.com/java_equivalents/unsigned_arithmetic.shtml
     * @param n1 First long
     * @param n2 Second long
     * @return is unsigned n1 less than unsigned n2
     */
    private boolean isLessThanUnsigned(long n1, long n2) {
        return (n1 < n2) ^ ((n1 < 0) != (n2 < 0));
    }
    
    private boolean isLessThanEqualUnsigned(long n1, long n2) {
        return isLessThanUnsigned(n1, n2) || n1 == n2;
    }
    
    /**
     * Find first/primary replica location
     * @param key
     * @return
     */
    private SlaveInfo findFirstReplica(String key) {
        // 64-bit hash of the key
        long hashedKey = hashTo64bit(key.toString());
        
        return slaveMap.get(hashedKey);
    }
    
    /**
     * Find the successor of firstReplica to put the second replica
     * @param firstReplica
     * @return
     */
    private SlaveInfo findSuccessor(SlaveInfo firstReplica) {
        return slaveMap.getSuccessor(firstReplica.getSlaveID());
    }
    
    private void sendMessage(KVMessage msg, SlaveInfo replicaSlave,
                             SlaveInfo successorSlave) throws KVException {
        Socket replica = replicaSlave.connectHost();
        Socket successor = successorSlave.connectHost();
        msg.sendMessage(replica);
        msg.sendMessage(successor);
        replicaSlave.closeHost(replica);
        successorSlave.closeHost(successor);
    }
    
    /**
     * Synchronized method to perform 2PC operations one after another
     *
     * @param msg
     * @param isPutReq
     * @return True if the TPC operation has succeeded
     * @throws KVException
     */
    private boolean globalAbort = false;
    private CyclicBarrier globalAbortBarrier = new CyclicBarrier(2);
    public class TPCOperationThread extends Thread {
        private KVMessage msg = null;
        private SlaveInfo slave = null;
        private KVException exception = null;
        private boolean isSuccessor;

        public TPCOperationThread(SlaveInfo slave, KVMessage msg, boolean isSuccessor) {
            this.slave = slave;
            this.msg = msg;
            this.isSuccessor = isSuccessor;
        } 

        public KVException getException() {
            return exception;
        }

        public void run() {
            try {
                TPCOperationRun(msg, this, isSuccessor);
            } catch (KVException e) {
                exception = e;
            }
        }
    }
    private final Lock globalAbortLock = new ReentrantLock();
    public void setGlobalAbort(boolean b) {
        globalAbortLock.lock();
        globalAbort = b; 
        globalAbortLock.unlock();
    }

    public boolean getGlobalAbort() {
        globalAbortLock.lock();
        boolean t = globalAbort; 
        globalAbortLock.unlock();
        return t;
    }


    public void sendPrepare(KVMessage msg, Socket replica) throws KVException {
        msg = new KVMessage(msg);
        msg.setTpcOpId(getNextTpcOpId());
        msg.sendMessage(replica);
    }

    public KVMessage waitForResponse(Socket replica) throws KVException {
        if (replica == null) 
            throw new KVException(KVMessage.RESP, KVError.TIMEOUT);
        KVMessage msg = new KVMessage(replica, TIMEOUT_MILLISECONDS);
        msg.setTpcOpId(getNextTpcOpId());
        return msg;
    }

    public KVMessage waitForAck(Socket replica) throws KVException {
        KVMessage msg = new KVMessage(replica, TIMEOUT_MILLISECONDS);
        msg.setTpcOpId(getNextTpcOpId());
        return msg;
    }

    public void sendAbort(Socket replica) throws KVException {
        KVMessage abortMsg = new KVMessage(KVMessage.ABORT);
        abortMsg.setTpcOpId(getNextTpcOpId());

        try {
            abortMsg.sendMessage(replica);
        } catch (KVException e2) {
            // ignore network error
        }
    }

    public KVMessage makeDecision(KVMessage replicaResp) throws KVException {
        KVMessage commitMsg = null;
        if (replicaResp.getMsgType().equals(KVMessage.ABORT))
            commitMsg = new KVMessage(KVMessage.ABORT);
        else
            commitMsg = new KVMessage(KVMessage.COMMIT);

        commitMsg.setTpcOpId(getNextTpcOpId());
        return commitMsg;
    }

    public KVMessage firstPhase(KVMessage msg, Object obj) throws KVException {
        return firstPhase(msg, obj, false);
    }

    public KVMessage firstPhase(KVMessage msg, Object obj, boolean manualGlobalAbort) throws KVException {
        Socket replica = null;
        
        // send PREPARE to slaves
        try {
            replica = slaveConnectHost(obj);
            sendPrepare(msg, replica);
        } catch (KVException e) {
            // ignore exceptions, coordinator does not
            // receive enough VOTE_COMMITs
            setGlobalAbort(true);
        }

        // wait for RESPONSE (READY/ABORT)
        KVMessage replicaResp = null;
        try {
            replicaResp = waitForResponse(replica);
        } catch (KVException e) {
            setGlobalAbort(true);
        }
        
        // manual global abort (for testing)
        if (manualGlobalAbort)
            setGlobalAbort(true);

        // wait for other threads
        barrier();

        if (getGlobalAbort()) {
            // timeout, send global ABORT
            if(replica != null)
            {
                slaveCloseHost(obj, replica);
                replica = slaveConnectHost(obj);
                sendAbort(replica);
            }
            return null;
        }

        if(replica != null)
            slaveCloseHost(obj, replica);
        return replicaResp;
    }

    public void secondPhase(String key, KVMessage replicaResp, 
            boolean isSuccessor, Object obj) throws KVException {
        String abortErr = null;
        Socket replica = null;

        // make decision
        KVMessage commitMsg = makeDecision(replicaResp);
        if (replicaResp.getMsgType().equals(KVMessage.ABORT))
            abortErr = replicaResp.getMessage();

        boolean retry = true;
        while (retry) {
            try {
                // send DECISION (COMMIT/ABORT)
                slaveSetSlave(obj, key, isSuccessor);
                replica = slaveConnectHost(obj);
                commitMsg.sendMessage(replica);

                // wait for ACK
                KVMessage ackResp = waitForAck(replica);
                if (!ackResp.getMsgType().equals(KVMessage.ACK)) {
                    throw new KVException(KVMessage.RESP, "Unknown Error: Expected ACK");
                }
                
                retry = false;
            } catch (KVException e) {
                // dont do anything, retry
            }
        }

        slaveCloseHost(obj, replica);

        if (abortErr != null) {
            throw new KVException(KVMessage.RESP, abortErr);
        }
    }

    /*
     * Methods Stubbed for testing
     */

    public void TPCOperationRun(KVMessage msg, Object obj, boolean isSuccessor) throws KVException {
        KVMessage replicaResp = firstPhase(msg, obj);

        if (replicaResp != null)
            secondPhase(msg.getKey(), replicaResp, isSuccessor, obj);
    }

    public Socket slaveConnectHost(Object obj) throws KVException {
        return ((TPCOperationThread) obj).slave.connectHost();
    }
    
    public void slaveCloseHost(Object obj, Socket socket) throws KVException {
        ((TPCOperationThread) obj).slave.closeHost(socket);
    }

    public void slaveSetSlave(Object obj, String key, boolean isSuccessor) {
        TPCOperationThread thread = (TPCOperationThread) obj;
        thread.slave = findFirstReplica(key); 
        if (isSuccessor)
            thread.slave = findSuccessor(thread.slave);
    }


    public void barrier() {
        try {
            globalAbortBarrier.await();
        } catch (Exception e) {
            e.printStackTrace();
        }
        globalAbortBarrier.reset();
    }


    /* 
     * performTPCOperation
     */

    public synchronized void performTPCOperation(KVMessage msg, boolean isPutReq) throws KVException {
        AutoGrader.agPerformTPCOperationStarted(isPutReq);
        String key = msg.getKey();
        String value = msg.getValue();
        String operation = msg.getMsgType();
        
        if (!operation.equals(KVMessage.PUTREQ) &&
            !operation.equals(KVMessage.DELREQ) ) {
            AutoGrader.agPerformTPCOperationFinished(isPutReq);
            throw new KVException(KVMessage.RESP, KVError.INVALID_OPERATION);
        }
        
        // Get slaves
        SlaveInfo replicaSlave = findFirstReplica(key);
        SlaveInfo successorSlave = findSuccessor(replicaSlave);
        
        TPCOperationThread replicaThread = new TPCOperationThread(replicaSlave, msg, false);
        TPCOperationThread successorThread = new TPCOperationThread(successorSlave, msg, true);
        replicaThread.start();
        successorThread.start();
        try {
            replicaThread.join();
            successorThread.join();
        } catch (InterruptedException e) {
        }

        if (replicaThread.getException() != null) {
            AutoGrader.agPerformTPCOperationFinished(isPutReq);
            throw replicaThread.getException();
        } else if (successorThread.getException() != null) {
            AutoGrader.agPerformTPCOperationFinished(isPutReq);
            throw successorThread.getException();
        }

        // Get write lock
        WriteLock lock = masterCache.getWriteLock(key);
        lock.lock();
        
        // add/del from KVCache
        if (operation.equals(KVMessage.PUTREQ)) {
            masterCache.put(key, value);
        } else { // DELREQ
            masterCache.del(key);
        }
        
        // release write lock
        lock.unlock();
        AutoGrader.agPerformTPCOperationFinished(isPutReq);
        
        return;
    }
    
    /**
     * Perform GET operation in the following manner:
     * - Try to GET from first/primary replica
     * - If primary succeeded, return Value
     * - If primary failed, try to GET from the other replica
     * - If secondary succeeded, return Value
     * - If secondary failed, return KVExceptions from both replicas
     *
     * @param msg Message containing Key to get
     * @return Value corresponding to the Key
     * @throws KVException
     */
    private class HandleGetThread extends Thread {
        private SlaveInfo slave = null;
        private String value = null;
        private String err = null;
        private String key = null;

        public HandleGetThread(SlaveInfo slave, String key) {
            this.slave = slave;
            this.key = key;
        }

        public void run() {
            Socket slaveSocket = null;
            try {
                slaveSocket = slave.connectHost();
                value = getFromSlave(key, slaveSocket);
                slave.closeHost(slaveSocket);
            } catch (KVException e) {
                err = slave.getErrorSegment(e);
                try {
                    slave.closeHost(slaveSocket);
                } catch (KVException e2) {
                }
            }
        }

        public String getValue() {
            return value; 
        }

        public String getErr() {
            return err; 
        }
    }

    public String handleGet(KVMessage msg) throws KVException {
        AutoGrader.aghandleGetStarted();
        
        String key = msg.getKey();
        
        // Get write lock
        WriteLock lock = masterCache.getWriteLock(key);
        lock.lock();
        
        // Try to get value from cache
        String value = masterCache.get(key);

        // release write lock
        lock.unlock();

        if (value != null) {
            AutoGrader.aghandleGetFinished();
            return value;
        }
        
        // get value from threads
        value = getValueFromThreads(key);
            
        // Get write lock
        lock = masterCache.getWriteLock(key);
        lock.lock();
        
        // write value to cache
        masterCache.put(key, value);
        
        // release write lock
        lock.unlock();
        AutoGrader.aghandleGetFinished();
        return value;
    }

    public String getValueFromSlaves(String replicaValue, String replicaErr, 
            String successorValue, String successorErr) throws KVException {
        // if not on both slaves, get error messages and throw excetion
        String value = null;
        if (replicaValue == null && successorValue == null) {
            String err = "";
            if (replicaValue == null)
                err += replicaErr;
            if (successorValue== null) {
                if (err.length() > 0)
                    err += "\n";
                err += successorErr;
            }
            throw new KVException(KVMessage.RESP, err);
        } else if (replicaValue != null) {
            value = replicaValue;
        } else if (successorValue != null) {
            value = successorValue;
        } else {
            throw new KVException(KVMessage.RESP, "Unknown Error: expected value");
        }
        return value;
    }

    public String getValueFromThreads(String key) throws KVException {
        // Get value from slaves
        SlaveInfo replicaSlave = findFirstReplica(key);
        SlaveInfo successorSlave = findSuccessor(replicaSlave);
        
        HandleGetThread replicaThread = new HandleGetThread(replicaSlave, key);
        HandleGetThread successorThread = new HandleGetThread(successorSlave, key);

        // start threads
        replicaThread.start();
        successorThread.start();

        // wait for threads to finish
        try {
            replicaThread.join();
            successorThread.join();
        } catch (InterruptedException e) {
            throw new KVException(KVMessage.RESP, e.getMessage());
        }

        return getValueFromSlaves(
                    replicaThread.getValue(), 
                    replicaThread.getErr(),
                    successorThread.getValue(),
                    successorThread.getErr()
                );
    
    }
    
    public String getFromSlave(String key, Socket slaveSocket) throws KVException {
        String value = null;
        KVMessage slaveGetMsg = new KVMessage(KVMessage.GETREQ);
        slaveGetMsg.setKey(key);
        slaveGetMsg.sendMessage(slaveSocket);
        
        KVMessage slaveResponse = new KVMessage(slaveSocket, TIMEOUT_MILLISECONDS);
        value = slaveResponse.getValue();
        
        if (value == null) {
            throw new KVException(KVMessage.RESP, slaveResponse.getMessage());
        }
        
        return value;    
    }

    public int numSlavesRegistered() {
        return slaveMap.size(); 
    }
}
